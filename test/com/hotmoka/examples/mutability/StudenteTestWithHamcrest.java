package com.hotmoka.examples.mutability;

import static com.hotmoka.examples.mutability.Still.still;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

public class StudenteTestWithHamcrest {

	private Data birthday;
	private Studente fausto;
	
	@Before
	public void fixture() {
		birthday = new Data(1973, 1, 13);
		fausto = new Studente("Fausto", "Spoto", birthday);
	}

	@Test(expected = IllegalArgumentException.class)
	public void aNullNameShouldBeIllegal() {
		new Studente(null, "Spoto", birthday);
	}

	@Test(expected = IllegalArgumentException.class)
	public void aNullSurnameShouldBeIllegal() {
		new Studente("Fausto", null, birthday);
	}

	@Test(expected = IllegalArgumentException.class)
	public void aNullBorthdayShouldBeIllegal() {
		new Studente("Fausto", "Spoto", null);
	}

	@Test
	public void birthdayShouldNotBeNull() {
		assertNotNull(fausto.getDataDiNascita());
	}

	@Test
	public void birthdayShouldBeKept() {
		assertThat(fausto.getDataDiNascita(), is(equalTo(birthday)));
	}

	@Test
	public void birthdayShouldBeCopied() {
		assertThat(fausto.getDataDiNascita(), is(not(sameInstance(birthday))));
	}

	@Test
	public void birthdayShouldNotBeChangeableThroughConstructor() {
		birthday.next();

		assertThat
			("birthday must not be changed",
			fausto.getDataDiNascita(), is(still(equalTo(new Data(1973, 1, 13)))));
	}

	@Test
	public void birthdayShouldNotBeChangeableThroughAccessor() {
		fausto.getDataDiNascita().next();

		assertThat
			("birthday must not be changed",
			fausto.getDataDiNascita(), is(still(equalTo(new Data(1973, 1, 13)))));
	}
}