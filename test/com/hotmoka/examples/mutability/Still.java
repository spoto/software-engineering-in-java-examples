package com.hotmoka.examples.mutability;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

public class Still<T> extends TypeSafeMatcher<T> {

	private final Matcher<T> condition;

	private Still(Matcher<T> condition) {
		this.condition = condition;
	}

	@Override
	public void describeTo(Description description) {
		condition.describeTo(description);
	}

	@Override
	protected boolean matchesSafely(T value) {
		// il test è già fatto da condition, noi non
		// aggiungiamo nulla di nuovo
		return condition.matches(value);
	}

	public static <T> Matcher<T> still(Matcher<T> condition) {
		return new Still<T>(condition);
	}
}


