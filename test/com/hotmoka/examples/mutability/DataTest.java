package com.hotmoka.examples.mutability;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

// convention: end test class with Test
public class DataTest {

	private Data nye;
	private Data nyd;

	@Before
	public void fixture() {
		nye = new Data(2014, 12, 31);
		nyd = new Data(2015, 1, 1);
	}

	// convention: use "should" and explanatory names
	@Test
	public void nextOfNewYearsEveShouldBeNewYearsDay() {
		nye.next();

		assertEquals
			("next of new year's eve must be new year's day",
			nye, nyd);
	}

	@Test
	public void toStringShouldReturnItalianDate() {
		String s = nye.toString();

		assertEquals(s, "31 dicembre 2014");
	}

	@Test
	public void leapYearShouldBeAccepted() {
		// if an exception is thrown, the test will fail
		new Data(2012, 2, 29);
	}

	@Test(expected=DataIllegaleException.class)
	public void notLeapYearShouldThrowException() {
		new Data(2100, 2, 29);
	}

	@Test
	public void leapYearExceptionShouldBeThrown() {
		try {
			new Data(2100, 2, 29);
			fail();
		}
		catch (Exception e) {
			assertTrue(e instanceof DataIllegaleException);
			assertTrue(e.getMessage().contains("non è bisestile"));
		}
	}
}