package com.hotmoka.examples.mutability;

import static com.hotmoka.examples.mutability.NewYearsDay2015.newYearsDay2015;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

// convention: end test class with Test
public class DataTestWithHamcrest {

	private Data nye;

	@Before
	public void fixture() {
		nye = new Data(2014, 12, 31);
	}

	// convention: use "should" and explanatory names
	@Test
	public void nextOfNewYearsEveShouldBeNewYearsDay() {
		nye.next();

		assertThat
			("next of new year's eve must be new year's day",
			nye, is(newYearsDay2015()));
	}

	@Test
	public void toStringShouldReturnItalianDate() {
		assertThat(nye.toString(), is(allOf(notNullValue(), equalTo("31 dicembre 2014"))));
	}

	@Test
	public void leapYearShouldBeAccepted() {
		// if an exception is thrown, the test will fail
		new Data(2012, 2, 29);
	}

	@Test(expected=DataIllegaleException.class)
	public void notLeapYearShouldThrowException() {
		new Data(2100, 2, 29);
	}

	@Test
	public void leapYearExceptionShouldBeThrown() {
		try {
			new Data(2100, 2, 29);
			fail();
		}
		catch (Exception e) {
			assertThat(e, is(instanceOf(DataIllegaleException.class)));
			assertThat(e.getMessage(), is(allOf(notNullValue(), equalTo("Il 2100 non è bisestile"))));
		}
	}
}