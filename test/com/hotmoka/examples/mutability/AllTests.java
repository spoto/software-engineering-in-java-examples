package com.hotmoka.examples.mutability;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ DataTest.class, StudenteTest.class })
public class AllTests {
}

