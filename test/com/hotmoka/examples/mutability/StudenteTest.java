package com.hotmoka.examples.mutability;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;

import org.junit.Before;
import org.junit.Test;

public class StudenteTest {

	private Data birthday;
	private Studente fausto;
	
	@Before
	public void fixture() {
		birthday = new Data(1973, 1, 13);
		fausto = new Studente("Fausto", "Spoto", birthday);
	}

	@Test(expected = IllegalArgumentException.class)
	public void aNullNameShouldBeIllegal() {
		new Studente(null, "Spoto", birthday);
	}

	@Test(expected = IllegalArgumentException.class)
	public void aNullSurnameShouldBeIllegal() {
		new Studente("Fausto", null, birthday);
	}

	@Test(expected = IllegalArgumentException.class)
	public void aNullBorthdayShouldBeIllegal() {
		new Studente("Fausto", "Spoto", null);
	}

	@Test
	public void birthdayShouldNotBeNull() {
		assertNotNull(fausto.getDataDiNascita());
	}

	@Test
	public void birthdayShouldBeKept() {
		assertEquals(fausto.getDataDiNascita(), birthday);
	}

	@Test
	public void birthdayShouldBeCopied() {
		assertNotSame(fausto.getDataDiNascita(), birthday);
	}

	@Test
	public void birthdayShouldNotBeChangeableThroughConstructor() {
		birthday.next();

		assertEquals
			("birthday must not be changed",
			new Data(1973, 1, 13), fausto.getDataDiNascita());
	}

	@Test
	public void birthdayShouldNotBeChangeableThroughAccessor() {
		fausto.getDataDiNascita().next();

		assertEquals
			("birthday must not be changed",
			new Data(1973, 1, 13), fausto.getDataDiNascita());
	}
}