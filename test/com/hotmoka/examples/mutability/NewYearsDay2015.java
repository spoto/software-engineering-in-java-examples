package com.hotmoka.examples.mutability;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

public class NewYearsDay2015 extends TypeSafeMatcher<Data> {

	private final static Data nyd = new Data(2015, 1, 1);

	@Override
	public void describeTo(Description description) {
		description.appendText("not new year's day 2015");
	}

	@Override
	protected boolean matchesSafely(Data value) {
		return value.equals(nyd);
	}

	public static NewYearsDay2015 newYearsDay2015() {
		return new NewYearsDay2015();
	}
}