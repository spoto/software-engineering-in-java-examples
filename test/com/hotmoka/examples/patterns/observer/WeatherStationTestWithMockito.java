package com.hotmoka.examples.patterns.observer;

import static org.mockito.Matchers.anyFloat;
import static org.mockito.Matchers.floatThat;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class WeatherStationTestWithMockito {

	private @Mock Observer observer;
	private WeatherStation station;

	@Before
	public void fixture() {
		station = new WeatherStation();
		station.register(observer);
	}

	@Test
	public void registeredObserversShouldBeNotified() {
		station.notifyObservers();
		verify(observer).update(anyFloat(), anyFloat(), anyFloat());
	}

	@Test
	public void observersRegisteredTwiceShouldBeNotifiedOnce() {
		station.register(observer);
		station.notifyObservers();
		verify(observer, times(1)).update(anyFloat(), anyFloat(), anyFloat());
	}

	@Test
	public void removedObserversShouldNotBeNotified() {
		station.remove(observer);
		station.notifyObservers();
		verify(observer, never()).update(anyFloat(), anyFloat(), anyFloat());
	}

	@Test
	public void measurementChangeShouldNotifyObservers() {
		station.measurementsChanged();
		verify(observer).update(anyFloat(), anyFloat(), anyFloat());
	}

	@Test
	public void humidityShouldBeBetween0And100() {
		station.measurementsChanged();
		verify(observer).update(anyFloat(), floatThat(new TypeSafeMatcher<Float>() {

			@Override
			public void describeTo(Description description) {
			}

			@Override
			protected boolean matchesSafely(Float value) {
				return 0.0f <= value && value <= 100.0f;
			}
			
		}), anyFloat());
	}

	@Test(expected = RuntimeException.class)
	public void exceptionInObserverShouldPropagateToObservable() {
		doThrow(new RuntimeException())
			.when(observer)
			.update(anyFloat(), anyFloat(), anyFloat());

		station.notifyObservers();
	}




}