package com.hotmoka.examples.circuits;

import static org.junit.Assert.*;
import org.junit.Test;

public class NotTest {

	@Test
	public void testEquals() {
		assertEquals(new Variable("v"), new Not(new Not(new Variable("v"))));
	}

	@Test
	public void testNotEquals() {
		assertFalse(new Variable("v").equals(new Not(new Variable("v"))));
		assertFalse(new Variable("v").equals(new Not(new Not(new Variable("b")))));
	}

	@Test
	public void testFreeVariables() {
		Variable[] vars = new Not(new Variable("v")).variables();
		assertArrayEquals(new Variable[] { new Variable("v") }, vars);
	}

	@Test
	public void testIsTrueIn() {
		Assignment assignment = new Assignment("v", "w");
		Not notV = new Not(new Variable("v"));
		assertFalse(notV.isTrueIn(assignment));
		Not notW = new Not(new Variable("w"));
		assertFalse(notW.isTrueIn(assignment));
		Not notPippo = new Not(new Variable("pippo"));
		assertTrue(notPippo.isTrueIn(assignment));
	}
}