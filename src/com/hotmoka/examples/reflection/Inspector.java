package com.hotmoka.examples.reflection;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;


public class Inspector {

	private static void inspect(Class<?> clazz) {
		System.out.println("Class " + clazz.getName() + ":");

		for (Field field: clazz.getDeclaredFields()) {
			System.out.println("  field " + field.getName() + " of type " + field.getType());
			printAnnotations(field.getAnnotations());
		}

		for (Constructor<?> con: clazz.getDeclaredConstructors()) {
			System.out.println("  constructor with arguments " + Arrays.toString(con.getParameterTypes()));
			printAnnotations(con.getAnnotations());
		}

		for (Method method: clazz.getDeclaredMethods()) {
			System.out.println("  method " + method.getName() + " with arguments " + Arrays.toString(method.getParameterTypes()));
			printAnnotations(method.getAnnotations());
		}
	}

	private static void printAnnotations(Annotation[] annotations) {
		if (annotations.length > 0) {
			System.out.print("    with annotations ");
			for (Annotation annotation: annotations)
				System.out.print(annotation + " ");
			System.out.println();
		}
	}

	public static void main(String[] args) {
		inspect(MyHappyClass.class);
	}
}
