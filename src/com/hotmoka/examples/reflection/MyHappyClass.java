package com.hotmoka.examples.reflection;

public class MyHappyClass {

	public MyHappyClass father;
	private @Ugly int id;
	public @Nice float f;

	public @Nice @Ugly MyHappyClass(int id, long l) {
		this.id = id;
		this.f = l;
	}
	
	public MyHappyClass(MyHappyClass father) {
		this.father = father;
	}
	
	public @Ugly void m() {}

	public int n(int a, @Nice float b) {
		return a + id;
	}
}