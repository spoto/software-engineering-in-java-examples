package com.hotmoka.examples.reflection;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;


public class Creator {
	public static void main(String[] args)
			throws SecurityException, NoSuchMethodException,
				IllegalArgumentException, InstantiationException,
				IllegalAccessException, InvocationTargetException {

		// creates a MyHappyClass and calls one of its methods
		Class<MyHappyClass> clazz = MyHappyClass.class;
		Constructor<MyHappyClass> con = clazz.getConstructor(int.class, long.class);
		MyHappyClass instance = con.newInstance(13, 17L);
		Method method = clazz.getMethod("n", int.class, float.class);
		int result = (Integer) method.invoke(instance, 100, 3.1415f);
		System.out.println("result = " + result);
	}
}