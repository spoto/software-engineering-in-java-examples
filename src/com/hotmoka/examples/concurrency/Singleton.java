package com.hotmoka.examples.concurrency;

/**
 * The implementation of the singleton pattern in a concurrent setting
 */

public class Singleton {
	private static Singleton singleton;

	private Singleton() {
		// very heavy and memory hungry computation here!
	}

	public static Singleton get() {
		// double checking: avoid useless synchronizations
		if (singleton == null)
			synchronized (Singleton.class) {
				if (singleton == null)
					singleton = new Singleton();
				// synchronization guarantees safe publication of the singleton
			}

		return singleton;
	}
}