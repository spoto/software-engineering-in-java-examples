package com.hotmoka.examples.concurrency;
import java.util.HashSet;
import java.util.Set;

// This goes in livelock!!!
public class SetOfTreesMultithreaded {
	private final Set<Tree> trees = new HashSet<Tree>();
	private final static int HEIGHT = 6;
	private final static int THREADS = 4;

	public SetOfTreesMultithreaded(final int howMany) throws InterruptedException {

		class Slave extends Thread {

			private Slave() {
				super("slave");
			}

			@Override
			public void run() {
				while (trees.size() < howMany)
					trees.add(new Tree(HEIGHT));
			}
		}

		Slave[] slaves = new Slave[THREADS];
		for (int pos = 0; pos < THREADS; pos++)
			(slaves[pos] = new Slave()).start();

		for (Slave slave: slaves)
			slave.join();

		System.out.println("Built " + trees.size() + " distinct trees");
	}

	public static void main(String[] args) throws InterruptedException {
		long start = System.currentTimeMillis();
		new SetOfTreesMultithreaded(200000);
		long elapsed = System.currentTimeMillis() - start;
		System.out.println("Time elapsed: " + elapsed + "ms");
	}
}
