package com.hotmoka.examples.concurrency;
import java.util.HashSet;
import java.util.Set;

public class SetOfTrees {
	private final Set<Tree> trees = new HashSet<Tree>();
	private final static int HEIGHT = 6;

	public SetOfTrees(int howMany) {
		while (trees.size() < howMany)
			trees.add(new Tree(HEIGHT));

		System.out.println("Built " + trees.size() + " distinct trees");
	}

	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		new SetOfTrees(200000);
		long elapsed = System.currentTimeMillis() - start;
		System.out.println("Time elapsed: " + elapsed + "ms");
	}
}
