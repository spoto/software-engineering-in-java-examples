package com.hotmoka.examples.concurrency;
import java.io.File;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Files {
	public final static int SIZE = 10;
	public final static int NUM_CONSUMERS = 5;

	public static void main(String[] args) {
		BlockingQueue<File> queue = new LinkedBlockingQueue<File>(SIZE);

		// as many producers as there are file names
		for (int pos = 0; pos < args.length; pos++)
			new FileCrawler(queue, new File(args[pos])).start();

		// a fixed number of consumers
		for (int num = 0; num < NUM_CONSUMERS; num++)
			new FileIndexer(queue, num).start();
	}
}