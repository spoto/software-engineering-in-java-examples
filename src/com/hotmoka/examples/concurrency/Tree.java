package com.hotmoka.examples.concurrency;
import java.util.Random;

public final class Tree {
	private final int root;
	private final Tree left;
	private final Tree right;

	private final static Random random = new Random();

	public Tree(int root, Tree left, Tree right) {
		this.root = root;
		this.left = left;
		this.right = right;
	}

	public Tree(int height) {
		this(
			random.nextInt(100),
			height > 1 ? new Tree(height - 1) : null,
			height > 1 ? new Tree(height - 1) : null);
	}

	@Override
	public boolean equals(Object other) {
		if (other == null || getClass() != other.getClass())
			return false;

		Tree otherAsTree = (Tree) other;

		return root == otherAsTree.root && equalsTrees(left, otherAsTree.left)
				&& equalsTrees(right, otherAsTree.right);
	}

	private static boolean equalsTrees(Tree first, Tree second) {
		if (first == null || second == null)
			return first == second;
		else
			return first.equals(second);
	}

	@Override
	public int hashCode() {
		return root;
	}
}
