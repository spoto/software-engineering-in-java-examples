package com.hotmoka.examples.concurrency;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;

// This does not livelock but there is a guarantee
// that the number of trees will actually be 200000
public class SetOfTreesMultithreadedConcurrentDebuggedEfficient {
	private final Set<Tree> trees;
	private final static int HEIGHT = 6;
	private final static int THREADS = 4;

	public SetOfTreesMultithreadedConcurrentDebuggedEfficient(final int howMany) throws InterruptedException {

		final ConcurrentMap<Tree, Boolean> seen = new ConcurrentHashMap<Tree, Boolean>();
		final AtomicInteger counter = new AtomicInteger();

		class Slave extends Thread {

			private Slave() {
				super("slave");
			}

			@Override
			public void run() {
				while (counter.incrementAndGet() <= howMany)
					while (seen.put(new Tree(HEIGHT), Boolean.TRUE) != null);
			}
		}

		Slave[] slaves = new Slave[THREADS];
		for (int pos = 0; pos < THREADS; pos++)
			(slaves[pos] = new Slave()).start();

		for (Slave slave: slaves)
			slave.join();

		trees = seen.keySet();

		System.out.println("Built " + trees.size() + " distinct trees");
	}

	public static void main(String[] args) throws InterruptedException {
		long start = System.currentTimeMillis();
		new SetOfTreesMultithreadedConcurrentDebuggedEfficient(200000);
		long elapsed = System.currentTimeMillis() - start;
		System.out.println("Time elapsed: " + elapsed + "ms");
	}
}
