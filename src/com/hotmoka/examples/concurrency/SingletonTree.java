package com.hotmoka.examples.concurrency;

import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * The implementation of the generalized singleton pattern in a concurrent setting
 */

public class SingletonTree {
	private final boolean value;
	private final SingletonTree left;
	private final SingletonTree right;

	private SingletonTree(boolean value, SingletonTree left, SingletonTree right) {
		this.value = value;
		this.left = left;
		this.right = right;
	}

	@Override
	public boolean equals(Object other) {
		if (other instanceof SingletonTree) {
			SingletonTree otherAsTree = (SingletonTree) other;

			return value == otherAsTree.value
				&& Objects.equals(left, otherAsTree.left)
				&& Objects.equals(right, otherAsTree.right);
		}

		return false;
	}

	@Override
	public int hashCode() {
		return (value ? 0 : 1) | ((Objects.hashCode(left) ^ Objects.hashCode(right)) << 1);
	}

	private final static ConcurrentMap<SingletonTree, SingletonTree> memory = new ConcurrentHashMap<SingletonTree, SingletonTree>();

	public static SingletonTree mk(boolean value, SingletonTree left, SingletonTree right) {
		SingletonTree result = new SingletonTree(value, left, right);

		// the following operation is atomic
		SingletonTree old = memory.putIfAbsent(result, result);

		if (old != null)
			// we do not create an equal tree but use the previous stored in memory
			result = old;

		return result;
	}
}