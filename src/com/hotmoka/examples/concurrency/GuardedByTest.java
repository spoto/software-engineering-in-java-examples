package com.hotmoka.examples.concurrency;

import com.juliasoft.julia.checkers.guardedBy.GuardedBy;
import com.juliasoft.julia.checkers.guardedBy.Holding;

public class GuardedByTest {

	@GuardedBy("this")
	private int counter;

	private Object lock = new Object();

	@GuardedBy("this.lock")
	private long counter2;

	@GuardedBy("com.hotmoka.examples.concurrency.GuardedByTest.class")
	private static int counter3;

	private static Object lock4 = new Object();

	@GuardedBy("lock4")
	private static int counter4 = 13;

	private final Object[] locks = new Object[10];
	private int next;

	@GuardedBy("itself")
	private final Object lock5 = new Object();

	@GuardedBy("itself")
	private final static String lock6 = new String();

	@GuardedBy("this.getLock()")
	private int counter5;

	public GuardedByTest() {
		// this is just to make it harder...
		// these assignments are irrelevant for the @GuardedBy annotations
		this.counter = 1;
		this.counter5 = 17;
		this.counter5 += counter;
	}

	public synchronized void incrementCounter() {
		counter++;
	}

	public void decrementCounter() {
		synchronized (this) {
		        test1(this);
                        this.test2();
			counter--;
		}
	}

        private static void test1(Object o) {}
        private void test2() {};

	public boolean checkCounterIs0() {
		Object copy = this;

		synchronized (copy) {
			return counter == 0;
		}
	}

	public void incrementOtherCounter(GuardedByTest other) {
		GuardedByTest copy = other;

		synchronized (copy) {
			incrementCounterAux(other);
		}
	}

	// by making the following public, counter is no more guarded by "itself"
    @Holding("other")
	private void incrementCounterAux(@GuardedBy("itself") GuardedByTest other) {
		other.counter += 2;
	}

	public void incrementCounter2() {
		synchronized (lock) {
			counter2++;
		}
	}

	public void decrementCounter2() {
		synchronized (lock) {
			counter2--;
		}
	}

	public boolean checkCounter2Is0() {
		Object copy = lock;

		synchronized (copy) {
			// this is fine, but is this what we want?
			boolean result = counter2 == 0;
			lock = new Object();
			// if lock is assigned before computing result, we get a warning

			return result;
		}
	}
    
	public void incrementOtherCounter2(GuardedByTest other) {
		GuardedByTest copy = other;

		synchronized (copy.lock) {
			incrementCounter2Aux(other);
		}
	}

	private static void incrementCounter2Aux(GuardedByTest other) {
		other.counter2 += 2;
	}

	public void incrementCounter3() {
		synchronized (GuardedByTest.class) {
			counter3++;
		}
	}

	@Holding("GuardedByTest.class")
	public static synchronized void decrementCounter3() {
		counter3--;
	}

	public void incrementCounter4() {
		synchronized (lock4) {
			counter4++;
		}
	}

	public static void decrementCounter4() {
		synchronized (lock4) {
			synchronized (GuardedByTest.class) {
				decrementCounter4Aux();
			}
		}
	}

	@Holding({"lock4", "lock6"})
	private static void decrementCounter4Aux() {
		counter4--;
	}

	// if the following is not private, there is no guarantee that it is deterministic
	private Object getLock() {
		//return new Object();
		return locks[next % locks.length];
	}

	public void incrementCounter5() {
		synchronized (getLock()) {
			//next++; // => no more locked by getLock()
			counter5++;
			next++; // fine
		}
	}

	public void decrementCounter5() {
		synchronized (getLock()) {
			decrementCounter5Aux();
		}
	}

	private void decrementCounter5Aux() {
		counter5--;
	}

	public void accessLock5() {
		synchronized (lock5) {
			lock5.hashCode();
		}
	}

	public static void accessLock6() {
		synchronized (lock6) {
			lock6.hashCode();
		}
	}

	public synchronized void parameterTest1(@GuardedBy("this") Object o1, @GuardedBy("lock") String o2, @GuardedBy("itself") String o3) {
		System.out.println(o1);

		synchronized (lock) {
			System.out.println(o2);
		}

		synchronized (o3) {
			System.out.println(o3 + " ");
		}
	}
}
