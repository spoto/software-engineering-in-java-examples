package com.hotmoka.examples.concurrency;
import java.io.File;
import java.util.Random;
import java.util.concurrent.BlockingQueue;


public class FileIndexer extends Thread {
	private final BlockingQueue<File> queue;
	private final int id;

	public FileIndexer(BlockingQueue<File> queue, int id) {
		this.queue = queue;
		this.id = id;
	}

	@Override
	public void run() {
		try {
			while (true)
				indexFile(queue.take());
		}
		catch (InterruptedException e) {}
	}

	private void indexFile(File file) {
		System.out.println("Indexer #" + id + " starts processing " + file);

		try {
			// very long processing of the file goes here....
			Thread.sleep(Math.abs(new Random().nextLong() % 1000));
		}
		catch (InterruptedException e) {}

		System.out.println("Indexer #" + id + " finished processing " + file);
	}
}