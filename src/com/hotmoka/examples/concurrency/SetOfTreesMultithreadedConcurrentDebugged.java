package com.hotmoka.examples.concurrency;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

// This does not livelock, puts exactly 200000 trees in the set but is slow!
public class SetOfTreesMultithreadedConcurrentDebugged {
	private final Set<Tree> trees;
	private final static int HEIGHT = 6;
	private final static int THREADS = 4;

	public SetOfTreesMultithreadedConcurrentDebugged(final int howMany) throws InterruptedException {

		final ConcurrentMap<Tree, Boolean> seen = new ConcurrentHashMap<Tree, Boolean>();

		class Slave extends Thread {

			private Slave() {
				super("slave");
			}

			@Override
			public void run() {
				while (seen.size() < howMany)
					synchronized (seen) {
						if (seen.size() < howMany)
							seen.put(new Tree(HEIGHT), Boolean.TRUE);
					}
			}
		}

		Slave[] slaves = new Slave[THREADS];
		for (int pos = 0; pos < THREADS; pos++)
			(slaves[pos] = new Slave()).start();

		for (Slave slave: slaves)
			slave.join();

		trees = seen.keySet();

		System.out.println("Built " + trees.size() + " distinct trees");
	}

	public static void main(String[] args) throws InterruptedException {
		long start = System.currentTimeMillis();
		new SetOfTreesMultithreadedConcurrentDebugged(200000);
		long elapsed = System.currentTimeMillis() - start;
		System.out.println("Time elapsed: " + elapsed + "ms");
	}
}
