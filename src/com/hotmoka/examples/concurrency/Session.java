package com.hotmoka.examples.concurrency;
import java.util.HashMap;
import java.util.Map;

/**
 * A "heavy" object.
 */

public class Session {
	private final Map<String, Object> map = new HashMap<String, Object>();

	public Object get(String key) {
		return map.get(key);
	}

	public void set(String key, Object value) {
		map.put(key, value);
	}
}