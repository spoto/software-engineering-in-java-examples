package com.hotmoka.examples.concurrency;
import java.util.Random;


public class ThreadLocality {
	private final ThreadLocal<Session> session;

	public ThreadLocality() {
		session = new ThreadLocal<Session>() {

			@Override
			protected Session initialValue() {
				return new Session();
			}
		};
	}

	public class Runner extends Thread {

		private final int id;

		public Runner(int id) {
			this.id = id;
		}

		@Override
		public void run() {
			session.get().set("hello", id);

			Random random = new Random();

			while (true) {
				System.out.println("For runner #" + id + " the session says " + session.get().get("hello"));
				try {
					Thread.sleep(Math.abs(random.nextInt() % 1000));
				} catch (InterruptedException e) {
				}
			}
		}
	}

	public static void main(String[] args) {
		ThreadLocality l = new ThreadLocality();

		l.new Runner(0).start();
		l.new Runner(13).start();
		l.new Runner(17).start();
	}
}