package com.hotmoka.examples.patterns.command;

public class Main {
	public static void main(String[] args) {
		Television tv = new Television();
		Controller ctrl = new Controller(tv);
		ctrl.pressKey(2);
		System.out.println(tv);
		ctrl.pressKey(3);
		System.out.println(tv);
		ctrl.pressKey(3);
		System.out.println(tv);
		ctrl.pressUndo();
		System.out.println(tv);
		ctrl.pressKey(6);
		System.out.println(tv);
		ctrl.pressUndo();
		System.out.println(tv);
	}
}