package com.hotmoka.examples.patterns.command;

public class IncrementChannelCommand implements Command {

	private final Television tv;

	public IncrementChannelCommand(Television tv) {
		this.tv = tv;
	}

	@Override
	public void execute() {
		tv.setChannel(tv.getChannel() + 1);
	}
}