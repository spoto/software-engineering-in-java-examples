package com.hotmoka.examples.patterns.command;

public interface Command {
	void execute();
}