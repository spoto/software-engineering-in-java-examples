package com.hotmoka.examples.patterns.command;

public class Controller {
	private final Command[] keys = new Command[7];
	private final Television tv;
	private Command undo = new EmptyCommand();

	public Controller(Television tv) {
		this.tv = tv;

		keys[0] = new SetChannelCommand(tv, 1);
		keys[1] = new SetChannelCommand(tv, 2);
		keys[2] = new SetChannelCommand(tv, 3);
		keys[3] = new IncrementChannelCommand(tv);
		keys[4] = keys[5] = keys[6] = new EmptyCommand();
	}

	public void pressKey(int key) {
		if (key >= 0 && key <= 6) {
			// if we undo this operation, we come back to the present state
			undo = new SetChannelCommand(tv, tv.getChannel());
			keys[key].execute();
		}
		else
			throw new IllegalArgumentException("key must be between 0 and 6");
	}

	public void pressUndo() {
		undo.execute();
	}

	public void programKey(int key, Command command) {
		if (key >= 0 && key <= 6)
			keys[key] = command;
		else
			throw new IllegalArgumentException("key must be between 0 and 6");
	}
}