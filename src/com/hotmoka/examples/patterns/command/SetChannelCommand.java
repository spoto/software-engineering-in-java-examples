package com.hotmoka.examples.patterns.command;

public class SetChannelCommand implements Command {

	private final Television tv;
	private final int channel;

	public SetChannelCommand(Television tv, int channel) {
		this.tv = tv;
		this.channel = channel;
	}

	@Override
	public void execute() {
		tv.setChannel(channel);
	}
}