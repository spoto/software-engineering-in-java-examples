package com.hotmoka.examples.patterns.template;

import java.util.Arrays;

public class Sort {

	public static <T extends Comparable<T>> void sort(T[] array) {
		while (swap(array));
	}

	private static <T extends Comparable<T>> boolean swap(T[] array) {
		boolean swapped = false;

		for (int pos = 0; pos < array.length - 1; pos++)
			// we call a template method of the Comparable interface
			if (array[pos].compareTo(array[pos + 1]) > 0) {
				T temp = array[pos];
				array[pos] = array[pos + 1];
				array[pos + 1] = temp;
				swapped = true;
			}
				
		return swapped;
	}

	public static void main(String[] args) {
		String[] array1 = new String[] { "ciao", "amico", "come", "va?" };
		Integer[] array2 = new Integer[] { 13, 8, -5, 13, 2 };
		sort(array1);
		sort(array2);

		System.out.println(Arrays.toString(array1));
		System.out.println(Arrays.toString(array2));
	}
}