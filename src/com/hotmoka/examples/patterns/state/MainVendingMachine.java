package com.hotmoka.examples.patterns.state;

public class MainVendingMachine {

	public static void main(String[] args) {
		VendingMachine machine = new VendingMachine();
		machine.insertMoney(0.50f);
		machine.insertMoney(1.00f);
		machine.insertMoney(2.00f);
		machine.pressDigit(2);
		machine.pressOK();
		machine.pressDigit(3);
		machine.pressOK();
		machine.insertMoney(2.00f);
		machine.insertMoney(2.00f);
		machine.insertMoney(2.00f);
		machine.pressOK();
		machine.pressClear();
	}
}