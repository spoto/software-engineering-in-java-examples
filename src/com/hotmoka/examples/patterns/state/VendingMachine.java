package com.hotmoka.examples.patterns.state;

public class VendingMachine {
	private float money;
	private int digits;
	private State state = State.START;

	private final static float[] costs =
	  { 4.50f, 4.00f, 5.30f, 8.20f, 4.65f, 7.00f, 6.00f, 7.90f, 5.40f, 6.10f,
		5.50f, 4.20f, 5.30f, 6.20f, 4.00f, 5.00f, 5.00f, 6.70f, 5.40f, 5.10f,
		6.60f, 4.30f, 5.30f, 7.10f, 4.10f, 6.00f, 5.00f, 3.40f, 5.00f, 6.40f,
		7.00f, 3.00f, 5.00f, 8.20f, 4.20f, 7.00f, 6.00f, 3.90f, 5.40f, 6.70f };

	// API against the user

	public void insertMoney(float money) {
		this.money += money;
		System.out.println(this);
	}

	public void pressDigit(int digit) {
		if (digit >= 0 && digit <= 9)
			state = state.pressDigit(this, digit);

		System.out.println(this);
	}

	public void pressOK() {
		state = state.pressOK(this);
		System.out.println(this);
	}

	public void pressClear() {
		state = state.pressClear(this);
		System.out.println(this);
	}

	// API against the state
	
	float getMoney() {
		return money;
	}

	int getDigits() {
		return digits;
	}

	void setDigits(int digits) {
		this.digits = digits;
	}

	boolean canBuy() {
		return digits >= 0 && digits < costs.length && costs[digits] <= money;
	}

	void buy() {
		money -= costs[digits];
		System.out.printf("Bought food for %.2f euros\n", costs[digits]);
	}

	@Override
	public String toString() {
		return String.format("[%s] display: %d, money: %.2f", state.toString(), digits, money);
	}
}