package com.hotmoka.examples.patterns.state.heroine.actions;

public class Heroine {
	private HeroineState state = StandingState.INSTANCE;

	public void handleInput(Input input) {
		HeroineState previous = state;
		state = state.handleInput(this, input);
		if (state != previous)
			state.enter(this);
	}

	public void update() {
		state.update(this);
	}

	protected void setYVelocity(int yVelocity) {}
	protected void setGraphics(String file) {}
}