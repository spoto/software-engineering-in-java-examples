package com.hotmoka.examples.patterns.state.heroine.actions;

public enum Input {
	PRESS_B, PRESS_DOWN, RELEASE_DOWN
}
