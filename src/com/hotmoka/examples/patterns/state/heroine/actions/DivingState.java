package com.hotmoka.examples.patterns.state.heroine.actions;

import static com.hotmoka.examples.patterns.state.heroine.actions.Input.*;

public class DivingState implements HeroineState {
	public final static DivingState INSTANCE = new DivingState();

	private DivingState() {}

	@Override
	public HeroineState handleInput(Heroine heroine, Input input) {
		if (input == PRESS_B) {
		}
		return null;
	}

	@Override
	public void update(Heroine heroine) {
		// TODO Auto-generated method stub

	}
	
	@Override
	public void enter(Heroine heroine) {
		heroine.setGraphics("image_dive.png");
	}
}