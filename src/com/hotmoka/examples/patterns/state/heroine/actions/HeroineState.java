package com.hotmoka.examples.patterns.state.heroine.actions;

public interface HeroineState {
	HeroineState handleInput(Heroine heroine, Input input);
	void update(Heroine heroine);
	void enter(Heroine heroine);
}