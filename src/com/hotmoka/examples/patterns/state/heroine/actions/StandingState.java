package com.hotmoka.examples.patterns.state.heroine.actions;

import static com.hotmoka.examples.patterns.state.heroine.actions.Input.*;

public class StandingState implements HeroineState {
	public final static StandingState INSTANCE = new StandingState();

	private StandingState() {}

	@Override
	public HeroineState handleInput(Heroine heroine, Input input) {
		if (input == PRESS_B) {
			heroine.setYVelocity(JUMP_VELOCITY);
			return JumpingState.INSTANCE;
		}
		else if (input == PRESS_DOWN)
			return new DuckingState();
		else
			return this;
	}

	@Override
	public void update(Heroine heroine) {}

	@Override
	public void enter(Heroine heroine) {
		heroine.setGraphics("image_stand.png");
	}

	private final static int JUMP_VELOCITY = 50;
}