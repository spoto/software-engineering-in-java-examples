package com.hotmoka.examples.patterns.state.heroine.actions;

import static com.hotmoka.examples.patterns.state.heroine.actions.Input.*;

public class DuckingState implements HeroineState {
	private int chargeTime;

	@Override
	public HeroineState handleInput(Heroine heroine, Input input) {
		if (input == RELEASE_DOWN)
			return StandingState.INSTANCE;
		else
			return this;
	}

	@Override
	public void update(Heroine heroine) {
		if (++chargeTime > MAX_CHARGE)
			superBomb();
	}

	@Override
	public void enter(Heroine heroine) {
		heroine.setGraphics("image_duck.png");
	}

	private final static int MAX_CHARGE = 100;
	private void superBomb() {}
}