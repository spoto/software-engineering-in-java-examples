package com.hotmoka.examples.patterns.state.heroine.actions;

import static com.hotmoka.examples.patterns.state.heroine.actions.Input.*;

public class JumpingState implements HeroineState {
	public final static JumpingState INSTANCE = new JumpingState();

	private JumpingState() {}

	@Override
	public HeroineState handleInput(Heroine heroine, Input input) {
		if (input == PRESS_DOWN)
			return DivingState.INSTANCE;
		else
			return this;
	}

	@Override
	public void update(Heroine heroine) {}

	@Override
	public void enter(Heroine heroine) {
		heroine.setGraphics("image_jump.png");
	}
}