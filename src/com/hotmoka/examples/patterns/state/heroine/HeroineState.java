package com.hotmoka.examples.patterns.state.heroine;

public interface HeroineState {
	HeroineState handleInput(Heroine heroine, Input input);
	void update(Heroine heroine);
}