package com.hotmoka.examples.patterns.state.heroine;

import static com.hotmoka.examples.patterns.state.heroine.Input.*;

public class StandingState implements HeroineState {
	public final static StandingState INSTANCE = new StandingState();

	private StandingState() {}

	@Override
	public HeroineState handleInput(Heroine heroine, Input input) {
		if (input == PRESS_B) {
			heroine.setYVelocity(JUMP_VELOCITY);
			heroine.setGraphics("image_jump.png");
			return JumpingState.INSTANCE;
		}
		else if (input == PRESS_DOWN) {
			heroine.setGraphics("image_duck.png");
			return new DuckingState();
		}
		else
			return this;
	}

	@Override
	public void update(Heroine heroine) {}

	private final static int JUMP_VELOCITY = 50;
}