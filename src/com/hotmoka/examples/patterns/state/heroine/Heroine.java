package com.hotmoka.examples.patterns.state.heroine;

public class Heroine {
	private HeroineState state = StandingState.INSTANCE;

	public void handleInput(Input input) {
		state = state.handleInput(this, input);
	}

	public void update() {
		state.update(this);
	}

	protected void setYVelocity(int yVelocity) {}
	protected void setGraphics(String file) {}
}