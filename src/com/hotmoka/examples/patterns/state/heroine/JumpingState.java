package com.hotmoka.examples.patterns.state.heroine;

import static com.hotmoka.examples.patterns.state.heroine.Input.*;

public class JumpingState implements HeroineState {
	public final static JumpingState INSTANCE = new JumpingState();

	private JumpingState() {}

	@Override
	public HeroineState handleInput(Heroine heroine, Input input) {
		if (input == PRESS_DOWN) {
			heroine.setGraphics("image_dive.png");
			return DivingState.INSTANCE;
		}
		else
			return this;
	}

	@Override
	public void update(Heroine heroine) {}
}