package com.hotmoka.examples.patterns.state.heroine;

public enum Input {
	PRESS_B, PRESS_DOWN, RELEASE_DOWN
}
