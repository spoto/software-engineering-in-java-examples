package com.hotmoka.examples.patterns.state.heroine;

import static com.hotmoka.examples.patterns.state.heroine.Input.*;

public class DuckingState implements HeroineState {
	private int chargeTime;

	@Override
	public HeroineState handleInput(Heroine heroine, Input input) {
		if (input == RELEASE_DOWN) {
			heroine.setGraphics("image_stand.png");
			return StandingState.INSTANCE;
		}
		else
			return this;
	}

	@Override
	public void update(Heroine heroine) {
		if (++chargeTime > MAX_CHARGE)
			superBomb();
	}

	private final static int MAX_CHARGE = 100;
	private void superBomb() {}
}