package com.hotmoka.examples.patterns.state.heroine;

import static com.hotmoka.examples.patterns.state.heroine.Input.*;

public class DivingState implements HeroineState {
	public final static DivingState INSTANCE = new DivingState();

	private DivingState() {}

	@Override
	public HeroineState handleInput(Heroine heroine, Input input) {
		if (input == PRESS_B) {
		}
		return null;
	}

	@Override
	public void update(Heroine heroine) {
		// TODO Auto-generated method stub

	}
}