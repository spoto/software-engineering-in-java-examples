package com.hotmoka.examples.patterns.state;

public enum State {

	START {

		@Override
		public State pressDigit(VendingMachine machine, int digit) {
			machine.setDigits(digit);
			return ONE_DIGIT;
		}
	},

	ONE_DIGIT {

		@Override
		public State pressDigit(VendingMachine machine, int digit) {
			machine.setDigits(machine.getDigits() * 10 + digit);
			return TWO_DIGITS;
		}
	},

	TWO_DIGITS {

		@Override
		public State pressOK(VendingMachine machine) {
			if (machine.canBuy()) {
				machine.buy();
				machine.setDigits(0);
				return START;
			}

			return TWO_DIGITS;
		}
	};

	public State pressDigit(VendingMachine machine, int digit) {
		return this;
	}

	public State pressClear(VendingMachine machine) {
		machine.setDigits(0);
		return START;
	}

	public State pressOK(VendingMachine machine) {
		return this;
	}
}