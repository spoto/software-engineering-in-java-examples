package com.hotmoka.examples.patterns.strategy;

public class RubberDuck extends AbstractDuck {

	public RubberDuck() {
		super(new Squeak(), new FlyNoWay());
	}

	@Override
	public String toString() {
		return "I am a rubbery duck!";
	}
}