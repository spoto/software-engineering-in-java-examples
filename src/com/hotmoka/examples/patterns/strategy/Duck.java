package com.hotmoka.examples.patterns.strategy;

public interface Duck {
	public void quack();
	public void fly();
}