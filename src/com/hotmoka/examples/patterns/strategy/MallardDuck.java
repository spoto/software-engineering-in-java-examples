package com.hotmoka.examples.patterns.strategy;

public class MallardDuck extends AbstractDuck {

	public MallardDuck() {
		super(new Quack(), new FlyWithWings());
	}

	@Override
	public String toString() {
		return "I am a mallard duck!";
	}
}