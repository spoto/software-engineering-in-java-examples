package com.hotmoka.examples.patterns.strategy;

public class FlyWithWings implements FlyBehavior {

	@Override
	public void fly() {
		System.out.println("flap flap");
	}
}