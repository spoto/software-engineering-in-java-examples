package com.hotmoka.examples.patterns.strategy;

public interface QuackBehavior {
	void quack();
}