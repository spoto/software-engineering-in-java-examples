package com.hotmoka.examples.patterns.strategy;

public class RedheadDuck extends AbstractDuck {

	public RedheadDuck() {
		super(new Quack(), new FlyWithWings());
	}

	@Override
	public String toString() {
		return "I am a readhead duck!";
	}
}