package com.hotmoka.examples.patterns.strategy;

public class DecoyDuck extends AbstractDuck {

	public DecoyDuck() {
		super(new MuteQuack(), new FlyNoWay());
	}

	@Override
	public String toString() {
		return "I am a pretty decoy duck";
	}
}