package com.hotmoka.examples.patterns.strategy;

public abstract class AbstractDuck implements Duck {
	private QuackBehavior quackBehavior;
	private FlyBehavior flyBehavior;

	protected AbstractDuck(QuackBehavior quackBehavior, FlyBehavior flyBehavior) {
		this.quackBehavior = quackBehavior;
		this.flyBehavior = flyBehavior;
	}

	@Override
	public abstract String toString();

	@Override
	public void quack() {
		quackBehavior.quack();
	}

	@Override
	public void fly() {
		flyBehavior.fly();
	}

	public void setQuackBehavior(QuackBehavior quackBehavior) {
		this.quackBehavior = quackBehavior;
	}

	public void setFlyBehavior(FlyBehavior flyBehavior) {
		this.flyBehavior = flyBehavior;
	}
}