package com.hotmoka.examples.patterns.strategy;

public class FlyingDecoyDuck extends DecoyDuck {

	protected FlyingDecoyDuck() {
		setFlyBehavior(new FlyWithWings());
	}

	@Override
	public String toString() {
		return "I am a flying decoy duck";
	}
}