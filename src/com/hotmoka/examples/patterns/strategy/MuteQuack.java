package com.hotmoka.examples.patterns.strategy;

public class MuteQuack implements QuackBehavior {

	@Override
	public void quack() {
	}
}