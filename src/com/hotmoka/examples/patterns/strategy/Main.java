package com.hotmoka.examples.patterns.strategy;

public class Main {

	public static void main(String[] args) {
		Duck duck1 = new MallardDuck();
		AbstractDuck duck2 = new DecoyDuck();
		animate(duck1);
		animate(duck2);

		// you can change a behavior at runtime!
		duck2.setFlyBehavior(new FlyBehavior() {
			
			@Override
			public void fly() {
				System.out.println("I'm flying as a rocket rock star!");
			}
		});

		animate(duck2);
	}

	private static void animate(Duck duck) {
		System.out.println(duck.toString());
		duck.fly();
		duck.quack();
	}
}