package com.hotmoka.examples.patterns.strategy;

public class FlyNoWay implements FlyBehavior {

	@Override
	public void fly() {
		System.out.println("not moving");
	}
}