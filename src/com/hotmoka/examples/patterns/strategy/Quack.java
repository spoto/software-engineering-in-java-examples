package com.hotmoka.examples.patterns.strategy;

public class Quack implements QuackBehavior {

	@Override
	public void quack() {
		System.out.println("quaaack!");
	}
}