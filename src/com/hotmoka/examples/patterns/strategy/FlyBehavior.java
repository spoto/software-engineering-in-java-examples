package com.hotmoka.examples.patterns.strategy;

public interface FlyBehavior {
	void fly();
}