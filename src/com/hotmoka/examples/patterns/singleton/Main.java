package com.hotmoka.examples.patterns.singleton;

public class Main {

	public static void main(String[] args) {
		SingletonTree tree1 = SingletonTree.mk(true, SingletonTree.mk(true, null, null), SingletonTree.mk(false, SingletonTree.mk(true, null, null), null));
		SingletonTree tree2 = SingletonTree.mk(true, SingletonTree.mk(true, null, null), SingletonTree.mk(false, SingletonTree.mk(true, null, null), null));

		// the static constructor has actually generated the same object, twice
		System.out.println(tree1 == tree2);
	}
}
