package com.hotmoka.examples.patterns.singleton;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * The implementation of the generalized singleton pattern in a monothreaded setting
 */

public class SingletonTree {
	private final boolean value;
	private final SingletonTree left;
	private final SingletonTree right;

	private SingletonTree(boolean value, SingletonTree left, SingletonTree right) {
		this.value = value;
		this.left = left;
		this.right = right;
	}

	@Override
	public boolean equals(Object other) {
		if (other instanceof SingletonTree) {
			SingletonTree otherAsTree = (SingletonTree) other;

			return value == otherAsTree.value
				&& Objects.equals(left, otherAsTree.left)
				&& Objects.equals(right, otherAsTree.right);
		}

		return false;
	}

	@Override
	public int hashCode() {
		return (value ? 0 : 1) | ((Objects.hashCode(left) ^ Objects.hashCode(right)) << 1);
	}

	private final static Map<SingletonTree, SingletonTree> memory = new HashMap<SingletonTree, SingletonTree>();

	public static SingletonTree mk(boolean value, SingletonTree left, SingletonTree right) {
		SingletonTree result = new SingletonTree(value, left, right);

		SingletonTree old = memory.get(result);
		if (old != null)
			// we do not create an equal tree but use the previous stored in memory
			result = old;
		else 
			memory.put(result, result);

		return result;
	}
}