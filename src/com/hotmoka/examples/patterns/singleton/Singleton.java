package com.hotmoka.examples.patterns.singleton;

/**
 * The implementation of the singleton pattern in a monothreaded setting
 */

public class Singleton {
	private static Singleton instance;

	private Singleton() {
		// very heavy and memory hungry computation here!
	}

	public static Singleton get() {
		if (instance == null)
			instance = new Singleton();

		return instance;
	}
}