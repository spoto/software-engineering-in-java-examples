package com.hotmoka.examples.patterns.iterator;

public interface Tokenization extends Iterable<String> {
}