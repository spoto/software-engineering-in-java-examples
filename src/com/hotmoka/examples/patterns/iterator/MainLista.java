package com.hotmoka.examples.patterns.iterator;

import java.util.Iterator;

public class MainLista {
	public static void main(String[] args) {
		Lista l = new Lista("ciao", new Lista("hello", null));

		for (Object o: l)
			System.out.println(o);

		Iterator<Object> it = l.iterator();
		while (it.hasNext())
			System.out.println(it.next());
	}
}
