package com.hotmoka.examples.patterns.iterator;

import java.util.Iterator;

public class Lista implements Iterable<Object> {
	public Object elemento;
	public Lista coda;

	public Lista(Object elemento, Lista coda) {
		this.elemento = elemento;
		this.coda = coda;
	}

	@Override
	public Iterator<Object> iterator() {
		return new java.util.Iterator<Object>() {

			private Lista cursore = Lista.this;

			@Override
			public boolean hasNext() {
				return cursore != null;
			}

			@Override
			public Object next() {
				Lista oldCursore = cursore;
				cursore = cursore.coda;

				return oldCursore.elemento;
			}

			@Override
			public void remove() {
			}
		};
	}
}
