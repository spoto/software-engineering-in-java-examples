package com.hotmoka.examples.patterns.iterator;

public class CharacterTokenization extends StringTokenization {

	public CharacterTokenization(String s, char c) {
		super(s, String.valueOf(c));
	}
}