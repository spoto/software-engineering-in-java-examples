package com.hotmoka.examples.patterns.iterator;

public class Main {

	public static void main(String[] args) {
		Tokenization tok = new StringTokenization("Questa$e'&una&prova!", "$&");
		for (String s: tok)
			System.out.println(s);

		System.out.println();

		Tokenization tok2 = new DoubleTokenization(tok, "ou");
		for (String s: tok2)
			System.out.println(s);
	}
}
