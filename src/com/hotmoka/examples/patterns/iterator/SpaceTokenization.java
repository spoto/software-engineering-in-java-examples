package com.hotmoka.examples.patterns.iterator;

public class SpaceTokenization extends CharacterTokenization {

	public SpaceTokenization(String s) {
		super(s, ' ');
	}
}