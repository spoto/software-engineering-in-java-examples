package com.hotmoka.examples.patterns.doublebuffer;

import com.hotmoka.examples.patterns.doublebuffer.Board.Coordinate;

public interface NextAliveProcessor {
	boolean isAliveNextAt(Coordinate coordinate);
}