package com.hotmoka.examples.patterns.doublebuffer;

import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Board {
	private final int width;
	private final int height;
	private boolean[][] current;
	private boolean[][] next;

	public Board(int width, int height, int howManyAlive) {
		if (width < 0 || height < 0)
			throw new IllegalArgumentException("The dimensions of the board cannot be negative");

		if (howManyAlive < 0 || howManyAlive > width * height)
			throw new IllegalArgumentException("The number of initially alive cells must be between 0 and " + (width * height));

		this.width = width;
		this.height = height;
		this.current = new boolean[width][height];
		this.next = new boolean[width][height];

		randomCoordinates()
			.distinct()
			.limit(howManyAlive)
			.forEach(coordinate -> current[coordinate.x][coordinate.y] = true);
	}

	public boolean isAliveAt(Coordinate coordinate) {
		return current[coordinate.x][coordinate.y];
	}

	private String printForCoordinate(Coordinate coordinate) {
		if (coordinate.x == width - 1)
			return current[coordinate.x][coordinate.y] ? "*\n" : " \n";
		else
			return current[coordinate.x][coordinate.y] ? "*" : " ";
	}

	@Override
	public String toString() {
		return allCoordinatesLexicographically()
				.map(this::printForCoordinate)
				.collect(Collectors.joining());
	}

	public void play(NextAliveProcessor processor) {
		while (true) {
			System.out.println(this);
			System.out.println("----------------------------------------");
			next(processor);
			
			try {
				Thread.sleep(500);
			}
			catch (InterruptedException e) {}
		}
	}

	private void next(NextAliveProcessor processor) {
		computeNext(processor);
		swap();
	}

	private void computeNext(NextAliveProcessor processor) {
		allCoordinatesLexicographically()
			.unordered()
			.parallel()
			.forEach(coordinate -> next[coordinate.x][coordinate.y] = processor.isAliveNextAt(coordinate));
	}

	private void swap() {
		boolean[][] temp = current;
		current = next;
		next = temp;
	}

	private Stream<Coordinate> randomCoordinates() {
		return new Random().ints(0, width * height)
			.mapToObj(i -> new Coordinate(i % width, i / width));
	}

	private Stream<Coordinate> allCoordinatesLexicographically() {
		return IntStream.range(0, width * height)
			.mapToObj(i -> new Coordinate(i % width, i / width));
	}

	public class Coordinate {
		public final int x;
		public final int y;

		public Coordinate(int x, int y) {
			this.x = x;
			this.y = y;
		}

		@Override
		public boolean equals(Object other) {
			return other instanceof Coordinate && ((Coordinate) other).x == x && ((Coordinate) other).y == y;
		}

		public Stream<Coordinate> around() {
			return IntStream.range(0, 9)
				.mapToObj(i -> new Coordinate(x + i % 3 - 1, y + i / 3 - 1))
				.filter(around -> around.x >= 0 && around.y >= 0
						&& around.x < width && around.y < height
						&& (around.x != x || around.y != y));
		}
	}
}