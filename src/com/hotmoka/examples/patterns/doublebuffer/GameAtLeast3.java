package com.hotmoka.examples.patterns.doublebuffer;

import com.hotmoka.examples.patterns.doublebuffer.Board.Coordinate;

public class GameAtLeast3 extends GameCounting {
	
	private boolean isAliveNextAt(Coordinate coordinate) {
		int aliveAround = howManyAliveAround(coordinate);
		return aliveAround >= 3;
	}

	public GameAtLeast3() {
		board.play(this::isAliveNextAt);
	}

	public static void main(String[] args) {
		new GameAtLeast3();
	}
}