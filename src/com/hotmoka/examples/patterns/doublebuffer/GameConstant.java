package com.hotmoka.examples.patterns.doublebuffer;

public class GameConstant extends Game {

	public GameConstant() {
		board.play(board::isAliveAt);
	}

	public static void main(String[] args) {
		new GameConstant();
	}
}