package com.hotmoka.examples.patterns.doublebuffer;

import java.util.Random;

public class GameOfLifeOld {
	private final static int HEIGHT = 20, WIDTH = 40;
	private boolean[][] current = new boolean[WIDTH][HEIGHT];
	private boolean[][] next = new boolean[WIDTH][HEIGHT];

	public GameOfLifeOld() {
		Random random = new Random();
		for (int counter = 0; counter < 100; counter++) {
			int y = random.nextInt(HEIGHT);
			int x = random.nextInt(WIDTH);
			current[x][y] = true;
		}
	}

	private void swap() {
		boolean[][] temp = current;
		current = next;
		next = temp;
	}

	private void computeNext() {
		for (int x = 0; x < WIDTH; x++)
			for (int y = 0; y < HEIGHT; y++) {
				int aliveAround = howManyAliveAround(x, y);
				next[x][y] = (current[x][y] && aliveAround == 2) || aliveAround == 3;
			}
	}

	private int howManyAliveAround(int x, int y) {
		int counter = 0;
		for (int xx = x - 1; xx <= x + 1; xx++)
			for (int yy = y - 1; yy <= y + 1; yy++)
				if (xx >= 0 && yy >= 0 && xx < WIDTH && yy < HEIGHT
					&& (xx != x || yy != y)
					&& current[xx][yy])
					counter++;

		return counter;
	}

	public void next() {
		computeNext();
		swap();
	}

	@Override
	public String toString() {
		String result = "";

		for (int y = 0; y < HEIGHT; y++) {
			for (int x = 0; x < WIDTH; x++)
				result += current[x][y] ? "*" : " ";

			result += "\n";
		}

		return result;
	}

	public static void main(String[] args) throws InterruptedException {
		GameOfLifeOld game = new GameOfLifeOld();

		while (true) {
			System.out.println(game);
			System.out.println("----------------------------------------");
			game.next();
			Thread.sleep(500);
		}
	}
}