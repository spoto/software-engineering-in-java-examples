package com.hotmoka.examples.patterns.doublebuffer;

import com.hotmoka.examples.patterns.doublebuffer.Board.Coordinate;

public abstract class GameCounting extends Game {
	protected int howManyAliveAround(Coordinate coordinate) {
		return (int) coordinate.around()
				.filter(board::isAliveAt)
				.count();
	}
}