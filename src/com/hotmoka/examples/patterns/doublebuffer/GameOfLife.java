package com.hotmoka.examples.patterns.doublebuffer;

import com.hotmoka.examples.patterns.doublebuffer.Board.Coordinate;

public class GameOfLife extends GameCounting {

	private boolean isAliveNextAt(Coordinate coordinate) {
		int aliveAround = howManyAliveAround(coordinate);
		return (board.isAliveAt(coordinate) && aliveAround == 2) || aliveAround == 3;
	}

	public GameOfLife() {
		board.play(this::isAliveNextAt);
	}

	public static void main(String[] args) {
		new GameOfLife();
	}
}