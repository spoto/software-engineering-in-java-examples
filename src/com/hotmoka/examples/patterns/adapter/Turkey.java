package com.hotmoka.examples.patterns.adapter;

public interface Turkey {
	public void gobble();
	public void fly(); // for short distances!
}