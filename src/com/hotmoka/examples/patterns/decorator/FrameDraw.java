package com.hotmoka.examples.patterns.decorator;

import com.hotmoka.examples.patterns.composite.Draw;

public class FrameDraw extends Draw {
	private final Draw child;

	public FrameDraw(Draw child) {
		this.child = child;
	}

	@Override
	public int getWidth() {
		return child.getWidth() + 2;
	}

	@Override
	public int getHeight() {
		return child.getHeight() + 2;
	}

	@Override
	public String getRow(int num) throws IndexOutOfBoundsException {
		int height = getHeight();

		if (num < 0 || num >= height)
			throw new IndexOutOfBoundsException();
		else if (num == 0 || num == height - 1) {
			StringBuilder sb = new StringBuilder();
			int width = getWidth();
			for (int pos = 0; pos < width; pos++)
				sb.append('@');
		
			return sb.toString();
		}
		else
			return '@' + child.getRow(num - 1) + '@';
	}
}