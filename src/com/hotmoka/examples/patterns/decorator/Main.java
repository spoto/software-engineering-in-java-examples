package com.hotmoka.examples.patterns.decorator;

import com.hotmoka.examples.patterns.composite.Draw;
import com.hotmoka.examples.patterns.composite.E;
import com.hotmoka.examples.patterns.composite.H;
import com.hotmoka.examples.patterns.composite.HorizontalDraw;
import com.hotmoka.examples.patterns.composite.L;
import com.hotmoka.examples.patterns.composite.O;
import com.hotmoka.examples.patterns.composite.Star;

public class Main {

	public static void main(String[] args) {
		Draw h = new H(), e = new E(), l = new L(), o = new O(), star = new Star();
		Draw hello = new HorizontalDraw(star, h, e, l, l, o, star);
		Draw frame = new FrameDraw(hello);
		System.out.println(frame);
		System.out.println(new HorizontalDraw(hello, frame));
	}
}