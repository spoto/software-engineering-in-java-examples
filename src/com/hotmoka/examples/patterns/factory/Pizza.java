package com.hotmoka.examples.patterns.factory;

import java.util.ArrayList;
import java.util.List;

public abstract class Pizza {
	private final String name;
	private final String dough;
	private final String sauce;
	private final List<String> toppings = new ArrayList<String>();

	protected Pizza(String name, String dough, String sauce) {
		this.name = name;
		this.dough = dough;
		this.sauce = sauce;
	}

	// the following are examples of template methods, that can be
	// redefined in order to specialize the algorithm of preparing a pizza
	protected void addToppings(String... toppings) {
		for (String topping: toppings)
			this.toppings.add(topping);
	}

	protected void bake() {
		System.out.println("Bake for 25 minutes");
	}

	protected void cut() {
		System.out.println("Cutting into diagonal slices");
	}

	protected void box() {
		System.out.println("Place in official box");
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(name);
		sb.append("\n  dough: ");
		sb.append(dough);
		sb.append("\n  sauce: ");
		sb.append(sauce);
		sb.append("\n  toppings: ");

		boolean first = true;
		for (String topping: toppings) {
			if (first)
				first = false;
			else
				sb.append(", ");

			sb.append(topping);
		}

		return sb.toString();
	}
}