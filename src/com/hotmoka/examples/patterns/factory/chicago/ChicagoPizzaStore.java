package com.hotmoka.examples.patterns.factory.chicago;

import com.hotmoka.examples.patterns.factory.Pizza;
import com.hotmoka.examples.patterns.factory.PizzaStore;

public class ChicagoPizzaStore extends PizzaStore {

	@Override
	protected Pizza createPizza(String type) {
		switch (type) {
		case "cheese":
			return new ChicagoStyleCheesePizza();
		case "clam":
			return new ChicagoStyleClamPizza();
		case "motown":
			return new MotownPizza();
		default:
			throw new IllegalArgumentException("Sorry, we don't bake " + type + " pizzas here");
		}
	}
}