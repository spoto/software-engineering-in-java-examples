package com.hotmoka.examples.patterns.factory.chicago;

import com.hotmoka.examples.patterns.factory.Pizza;

public class ChicagoStyleCheesePizza extends Pizza {

	public ChicagoStyleCheesePizza() {
		super(
			"Chicago Style Deep Dish Cheese Pizza",
			"Extra Thick Crust Dough",
			"Plum Tomato Sauce"
		);

		addToppings("Shredded Mozzarella Cheese");
	}

	@Override
	protected void cut() {
		// Chicagoans like squared things
		System.out.println("Cutting into square slices");
	}
}