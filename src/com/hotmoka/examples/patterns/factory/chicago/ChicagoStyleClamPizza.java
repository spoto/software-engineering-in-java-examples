package com.hotmoka.examples.patterns.factory.chicago;

import com.hotmoka.examples.patterns.factory.Pizza;

public class ChicagoStyleClamPizza extends Pizza {

	public ChicagoStyleClamPizza() {
		super(
			"Chicago Style Deep Dish Clams Pizza",
			"Extra Thick Crust Dough",
			"Plum Tomato Sauce"
		);

		addToppings("Frozen Clams", "Oregano");
	}
}