package com.hotmoka.examples.patterns.factory.chicago;

import com.hotmoka.examples.patterns.factory.Pizza;

public class MotownPizza extends Pizza {

	public MotownPizza() {
		super(
			"Chicago Style Deep Dish Motown Pizza",
			"Extra Thick Crust Dough",
			"Plum Tomato Sauce"
		);

		addToppings("Shredded Cheddar Cheese", "herrings", "bacon", "ricotta");
	}

	@Override
	protected void cut() {
		// Chicagoans like squared things
		System.out.println("Cutting into square slices");
	}
}