package com.hotmoka.examples.patterns.factory.ny;

import com.hotmoka.examples.patterns.factory.Pizza;

public class NYStyleClamPizza extends Pizza {

	public NYStyleClamPizza() {
		super(
			"NY Style Sauce and Clams Pizza",
			"Thin Crust Dough",
			"Marinara Sauce"
		);

		addToppings("Fresh Clams", "Persil");
	}
}