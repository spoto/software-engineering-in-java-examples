package com.hotmoka.examples.patterns.factory.ny;

import com.hotmoka.examples.patterns.factory.Pizza;
import com.hotmoka.examples.patterns.factory.PizzaStore;

public class NYPizzaStore extends PizzaStore {

	@Override
	protected Pizza createPizza(String type) {
		switch (type) {
		case "cheese":
			return new NYStyleCheesePizza();
		case "clam":
			return new NYStyleClamPizza();
		default:
			throw new IllegalArgumentException("Sorry, we don't bake " + type + " pizzas here");
		}
	}
}