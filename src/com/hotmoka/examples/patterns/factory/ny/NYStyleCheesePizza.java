package com.hotmoka.examples.patterns.factory.ny;

import com.hotmoka.examples.patterns.factory.Pizza;

public class NYStyleCheesePizza extends Pizza {

	public NYStyleCheesePizza() {
		super(
			"NY Style Sauce and Cheese Pizza",
			"Thin Crust Dough",
			"Marinara Sauce"
		);

		addToppings("Grated Reggiano Chesse");
	}
}