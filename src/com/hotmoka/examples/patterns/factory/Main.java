package com.hotmoka.examples.patterns.factory;

import com.hotmoka.examples.patterns.factory.chicago.ChicagoPizzaStore;
import com.hotmoka.examples.patterns.factory.ny.NYPizzaStore;

public class Main {

	public static void main(String[] args) {
		PizzaStore ny = new NYPizzaStore();
		Pizza cheese1 = ny.orderPizza("cheese");

		PizzaStore chicago = new ChicagoPizzaStore();
		Pizza cheese2 = chicago.orderPizza("cheese");

		System.out.println("This is what you eat in NY when you order a cheese pizza:");
		System.out.println(cheese1);

		System.out.println("\nwhile you would eat this in Chicago instead:");
		System.out.println(cheese2);

		System.out.println("\nIn Chicago you can also order the delicious motown pizza, that looks like this:");
		System.out.println(chicago.orderPizza("motown"));

		System.out.println("\nwhile the same order in NY has unfortunate results:");
		System.out.println(ny.orderPizza("motown"));
	}
}