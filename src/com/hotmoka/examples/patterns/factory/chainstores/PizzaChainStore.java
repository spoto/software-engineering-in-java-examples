package com.hotmoka.examples.patterns.factory.chainstores;

import com.hotmoka.examples.patterns.factory.Pizza;
import com.hotmoka.examples.patterns.factory.PizzaStore;

public final class PizzaChainStore extends PizzaStore {
	private PizzaFactory factory;

	protected PizzaChainStore(PizzaFactory factory) {
		this.factory = factory;
	}

	public final void sell(PizzaFactory newFactory) {
		this.factory = newFactory;
	}

	@Override
	protected final Pizza createPizza(String type) {
		// we delegate to the factory, so that we are sure that
		// this store will adhere to the official baking rules of the chain
		// (also because this method is final)
		return factory.createPizza(type);
	}
}