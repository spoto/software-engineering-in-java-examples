package com.hotmoka.examples.patterns.factory.chainstores.ny;

import com.hotmoka.examples.patterns.factory.Pizza;
import com.hotmoka.examples.patterns.factory.chainstores.PizzaFactory;
import com.hotmoka.examples.patterns.factory.ny.NYStyleCheesePizza;
import com.hotmoka.examples.patterns.factory.ny.NYStyleClamPizza;

public class NYPizzaFactory extends PizzaFactory {

	@Override
	protected Pizza createPizza(String type) {
		switch (type) {
		case "cheese":
			return new NYStyleCheesePizza();
		case "clam":
			return new NYStyleClamPizza();
		default:
			throw new IllegalArgumentException("Sorry, we don't bake " + type + " pizzas here");
		}
	}
}