package com.hotmoka.examples.patterns.factory.chainstores;

import com.hotmoka.examples.patterns.factory.Pizza;
import com.hotmoka.examples.patterns.factory.chainstores.chicago.ChicagoPizzaFactory;
import com.hotmoka.examples.patterns.factory.chainstores.ny.NYPizzaFactory;

public class Main {

	public static void main(String[] args) {
		NYPizzaFactory nyStrategy = new NYPizzaFactory();
		PizzaChainStore alPacino = new PizzaChainStore(nyStrategy);
		Pizza cheese1 = alPacino.orderPizza("cheese");

		ChicagoPizzaFactory chicagoStrategy = new ChicagoPizzaFactory();
		PizzaChainStore venezia = new PizzaChainStore(chicagoStrategy);
		Pizza cheese2 = venezia.orderPizza("cheese");

		System.out.println("This is what you eat in NY when you order a cheese pizza:");
		System.out.println(cheese1);

		System.out.println("\nwhile you would eat this in Chicago instead:");
		System.out.println(cheese2);

		System.out.println("\nIn Chicago you can also order the delicious motown pizza, that looks like this:");
		System.out.println(venezia.orderPizza("motown"));

		System.out.println("\nThis store changes chain now");
		venezia.sell(nyStrategy);

		System.out.println("\nThey pretend to know anything about the motown pizza now:");
		System.out.println(venezia.orderPizza("motown"));
	}
}