package com.hotmoka.examples.patterns.factory.chainstores.chicago;

import com.hotmoka.examples.patterns.factory.Pizza;
import com.hotmoka.examples.patterns.factory.chainstores.PizzaFactory;
import com.hotmoka.examples.patterns.factory.chicago.ChicagoStyleCheesePizza;
import com.hotmoka.examples.patterns.factory.chicago.ChicagoStyleClamPizza;
import com.hotmoka.examples.patterns.factory.chicago.MotownPizza;

public class ChicagoPizzaFactory extends PizzaFactory {

	@Override
	protected Pizza createPizza(String type) {
		switch (type) {
		case "cheese":
			return new ChicagoStyleCheesePizza();
		case "clam":
			return new ChicagoStyleClamPizza();
		case "motown":
			return new MotownPizza();
		default:
			throw new IllegalArgumentException("Sorry, we don't bake " + type + " pizzas here");
		}
	}
}