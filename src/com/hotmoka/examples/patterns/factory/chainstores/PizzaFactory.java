package com.hotmoka.examples.patterns.factory.chainstores;

import com.hotmoka.examples.patterns.factory.Pizza;

public abstract class PizzaFactory {
	protected abstract Pizza createPizza(String type);
}