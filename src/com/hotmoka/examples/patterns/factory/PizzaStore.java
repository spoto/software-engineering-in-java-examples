package com.hotmoka.examples.patterns.factory;

public abstract class PizzaStore {

	public Pizza orderPizza(String type) {
		Pizza pizza = createPizza(type);

		pizza.bake();
		pizza.cut();
		pizza.box();

		return pizza;
	}

	// here is the factory method!
	protected abstract Pizza createPizza(String type);
}