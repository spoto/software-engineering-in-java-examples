package com.hotmoka.examples.patterns.observer;

public interface Observable {
	public void register(Observer observer);
	public void remove(Observer observer);
	public void notifyObservers();
}