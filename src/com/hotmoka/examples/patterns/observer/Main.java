package com.hotmoka.examples.patterns.observer;

public class Main {

	public static void main(String[] args) {
		WeatherStation station = new WeatherStation();

		// the new displays register themselves to the station,
		// so we do not need to keep a reference to them
		new CurrentConditionsDisplay(station);
		new ForecastDisplay(station);

		station.measurementsChanged();
		station.measurementsChanged();
		station.measurementsChanged();
	}
}