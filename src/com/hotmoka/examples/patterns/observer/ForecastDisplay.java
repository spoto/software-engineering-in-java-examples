package com.hotmoka.examples.patterns.observer;

public class ForecastDisplay implements Observer, Display {
	private float temperature;

	public ForecastDisplay(WeatherStation station) {
		station.register(this);
	}

	@Override
	public void display() {
		if (temperature > 25)
			System.out.println("Go to the beach!");
		else if (temperature > 0)
			System.out.println("Dress warm and go to work");
		else
			System.out.println("Stay home, it is going to be a cold cold day");
	}

	@Override
	public void update(float temperature, float humidity, float pressure) {
		this.temperature = temperature;

		// we immediately show the new data
		display();
	}
}