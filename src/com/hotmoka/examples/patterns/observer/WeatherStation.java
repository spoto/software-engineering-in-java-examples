package com.hotmoka.examples.patterns.observer;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class WeatherStation implements Observable {

	private final Set<Observer> observers = new HashSet<Observer>();

	private float temperature;
	private float humidity;
	private float pressure;

	@Override
	public void register(Observer observer) {
		observers.add(observer);
	}

	@Override
	public void remove(Observer observer) {
		observers.remove(observer);
	}

	@Override
	public void notifyObservers() {
		for (Observer observer: observers)
			observer.update(temperature, humidity, pressure);
	}

	private final Random random = new Random();

	public void measurementsChanged() {
		// in reality, these would come from some sensors
		temperature = random.nextFloat() * 40 - 10;
		humidity = random.nextFloat() * 50 + 50;
		pressure = random.nextFloat() * 50 + 990;

		notifyObservers();
	}
}