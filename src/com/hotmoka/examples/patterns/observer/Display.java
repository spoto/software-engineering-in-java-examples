package com.hotmoka.examples.patterns.observer;

public interface Display {
	public void display();
}