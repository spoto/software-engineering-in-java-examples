package com.hotmoka.examples.patterns.observer;

public class CurrentConditionsDisplay implements Observer, Display {
	private float temperature;
	private float humidity;

	public CurrentConditionsDisplay(WeatherStation station) {
		station.register(this);
	}

	@Override
	public void display() {
		System.out.printf("Current conditions: %.1f degrees and %.0f%% humidity\n", temperature, humidity);
	}

	@Override
	public void update(float temperature, float humidity, float pressure) {
		this.temperature = temperature;
		this.humidity = humidity;

		// we immediately show the new data
		display();
	}
}