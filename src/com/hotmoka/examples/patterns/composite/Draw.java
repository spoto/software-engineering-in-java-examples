package com.hotmoka.examples.patterns.composite;

public abstract class Draw {
	public abstract int getWidth();
	public abstract int getHeight();
	public abstract String getRow(int num) throws IndexOutOfBoundsException;

	@Override
	public final String toString() {
		int height = getHeight();

		StringBuilder sb = new StringBuilder();
		for (int row = 0; row < height; row++) {
			sb.append(getRow(row));
			sb.append('\n');
		}
		
		return sb.toString();
	}
}