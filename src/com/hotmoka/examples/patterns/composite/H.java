package com.hotmoka.examples.patterns.composite;

public class H extends Letter {

	@Override
	public String getRow(int num) throws IndexOutOfBoundsException {
		switch (num) {
		case 0:
			return "       ";
		case 4:
			return " ***** ";
		case 1: case 2: case 3: case 5: case 6: case 7:
			return " *   * ";
		default:
			throw new IndexOutOfBoundsException();
		}
	}
}