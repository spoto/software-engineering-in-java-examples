package com.hotmoka.examples.patterns.composite;

public class HorizontalDraw extends Draw {
	private final Draw[] children;

	public HorizontalDraw(Draw... children) {
		this.children = children;
	}

	@Override
	public int getWidth() {
		int width = 0;
		for (Draw child: children)
			width += child.getWidth();

		return width;
	}

	@Override
	public int getHeight() {
		int height = 0;
		for (Draw child: children)
			height = Math.max(height, child.getHeight());

		return height;
	}

	@Override
	public String getRow(int num) throws IndexOutOfBoundsException {
		StringBuilder sb = new StringBuilder();
		boolean atLeastOne = false;

		for (Draw child: children)
			try {
				sb.append(child.getRow(num));
				atLeastOne = true;
			}
			catch (IndexOutOfBoundsException e) {
				int width = child.getWidth();
				for (int pos = 0; pos < width; pos++)
					sb.append(' ');
			}

		if (atLeastOne)
			return sb.toString();
		else
			throw new IndexOutOfBoundsException();
	}
}