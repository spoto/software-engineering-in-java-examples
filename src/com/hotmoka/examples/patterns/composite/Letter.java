package com.hotmoka.examples.patterns.composite;

public abstract class Letter extends Draw {

	@Override
	public int getWidth() {
		return 7;
	}

	@Override
	public int getHeight() {
		return 8;
	}
}