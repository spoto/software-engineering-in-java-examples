package com.hotmoka.examples.patterns.composite;

public class O extends Letter {

	@Override
	public String getRow(int num) throws IndexOutOfBoundsException {
		switch (num) {
		case 0:
			return "       ";
		case 1: case 7:
			return " ***** ";
		case 2: case 3: case 4: case 5: case 6:
			return " *   * ";
		default:
			throw new IndexOutOfBoundsException();
		}
	}
}