package com.hotmoka.examples.patterns.composite;

public class Star extends Draw {

	@Override
	public int getWidth() {
		return 5;
	}

	@Override
	public int getHeight() {
		return 5;
	}

	@Override
	public String getRow(int num) throws IndexOutOfBoundsException {
		switch (num) {
		case 0: case 4:
			return "X   X";
		case 1: case 3:
			return " X X ";
		case 2:
			return "  X  ";
		default:
			throw new IndexOutOfBoundsException();
		}
	}
}