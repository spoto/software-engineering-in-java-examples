package com.hotmoka.examples.mutability;

public class DataIllegaleException extends RuntimeException {
	private static final long serialVersionUID = 8600607414203998642L;

	public DataIllegaleException(int anno, int mese, int giorno) {
		super(mese == 2 && giorno == 29 ?
			"Il " + anno + " non è bisestile":
			"La data " + giorno + "/" + mese + "/" + anno + " non esiste");
	}
}