package com.hotmoka.examples.mutability;

public class MainStudente {

	public static void main(String[] args) {
		Data d = new Data(1810, 8, 10);
		Studente camillo = new Studente("Camillo", "Benso", d);
		System.out.println(camillo.toString());
		Data nascita = camillo.getDataDiNascita();
		nascita.next();
		System.out.println(camillo.toString());
	}
}