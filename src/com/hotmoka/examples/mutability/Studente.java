package com.hotmoka.examples.mutability;

// copiando le date è immutabile!
public class Studente {
	private String nome;
	private String cognome;
	private Data nascita;

	public Studente(String nome, String cognome, Data nascita) {
		if (nome == null)
			throw new IllegalArgumentException("Il nome deve essere specificato");
		if (cognome == null)
			throw new IllegalArgumentException("Il cognome deve essere specificato");
		if (nascita == null)
			throw new IllegalArgumentException("La data di nascita deve essere specificata");

		this.nome = nome;
		this.cognome = cognome;
		this.nascita = new Data(nascita);  // copio l'oggetto!
	}

	@Override
	public String toString() {
		return "Mi chiamo " + nome + " " + cognome
				+ ", sono nato il " + nascita.toString();
	}

	public Data getDataDiNascita() {
		return new Data(nascita);  // copio l'oggetto
	}
}