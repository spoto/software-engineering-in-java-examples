package com.hotmoka.examples.mutability;

public class Data {

	// stato dell'oggetto Data
	private int giorno;
	private int mese; // da 1 a 12
	private int anno;

	// costruttore di copia
	public Data(Data originale) {
		this.giorno = originale.giorno;
		this.mese = originale.mese;
		this.anno = originale.anno;
	}

	// classe mutabile poiché public
	public void next() {
		giorno++;
		if (giorno > getGiorniDelMese()[mese - 1]) {
			giorno = 1;
			mese++;
			if (mese == 13) {
				mese = 1;
				anno++;
			}
		}
	}

	// non fa parte dello stato
	private static String[] meseInItaliano = {
		"gennaio", "febbraio", "marzo", "aprile",
		"maggio", "giugno", "luglio", "agosto",
		"settembre", "ottobre", "novembre", "dicembre"
	};

	public Data(int a, int m, int g) throws DataIllegaleException {
		anno = a;
		mese = m;
		giorno = g;

		controllaData();
	}

	private void controllaData() throws DataIllegaleException {
		int[] giorniDelMese = getGiorniDelMese();

		if (mese < 1 || mese > 12
				|| giorno < 1 || giorno > giorniDelMese[mese - 1])
			throw new DataIllegaleException(anno, mese, giorno);
	}

	private boolean bisestile() {
		return anno % 400 == 0 || (anno % 4 == 0 && anno % 100 != 0);
	}

	@Override
	public String toString() {
		return giorno + " " + meseInItaliano[mese - 1] + " " + anno;
	}

	private int[] getGiorniDelMese() {
		int[] giorniDelMese = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
		if (bisestile())
			giorniDelMese[1] = 29;

		return giorniDelMese;
	}

	@Override
	public boolean equals(Object other) {
		return other instanceof Data && equalsAux((Data) other);
	}

	private boolean equalsAux(Data other) {
		return giorno == other.giorno && mese == other.mese && anno == other.anno;
	}
}