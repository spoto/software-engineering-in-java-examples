package com.hotmoka.examples.circuits;

public class Variable extends Circuit {

	private final String name;

	public Variable(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}

	public String getName() {
		return name;
	}

	@Override
	public boolean isTrueIn(Assignment assignment) {
		// TODO
		return false;
	}

	@Override
	public Variable[] variables() {
		// TODO
		return null;
	}

	// ridefiniamo questo metodo per ottimizzare il test di equivalenza
	// fra due variabili. Inoltre questo evita il rischio di ricorsione
	// infinita tra {@link com.hotmoka.examples.circuit.Circuit.variables()}
	// e {@link com.hotmoka.examples.circuit.Circuit.equals()}
	@Override
	public final boolean equals(Object other) {
		if (other instanceof Variable)
			return name.equals(((Variable) other).name);
		else
			return super.equals(other);
	}
}