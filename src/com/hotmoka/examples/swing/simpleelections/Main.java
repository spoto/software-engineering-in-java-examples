package com.hotmoka.examples.swing.simpleelections;

import java.awt.EventQueue;

import com.hotmoka.examples.swing.simpleelections.controller.Controller;
import com.hotmoka.examples.swing.simpleelections.model.Model;
import com.hotmoka.examples.swing.simpleelections.view.HistogramElectionsFrame;
import com.hotmoka.examples.swing.simpleelections.view.NumericElectionsFrame;

public class Main {
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {

			@Override
			public void run() {
				MVC mvc = new MVC(new Model(), new Controller());

				new NumericElectionsFrame(mvc).setVisible(true);
				new NumericElectionsFrame(mvc).setVisible(true);
				new HistogramElectionsFrame(mvc).setVisible(true);
			}
		});
	}
}
