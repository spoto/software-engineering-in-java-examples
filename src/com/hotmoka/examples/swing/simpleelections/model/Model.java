package com.hotmoka.examples.swing.simpleelections.model;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;

import com.hotmoka.examples.swing.simpleelections.MVC;
import com.hotmoka.examples.swing.simpleelections.view.View;

public class Model {
	private MVC mvc;
	private final Map<String, Integer> votes = new HashMap<>();

	public void setMVC(MVC mvc) {
		this.mvc = mvc;
	}

	// 5: I need your state information
	public Iterable<String> getParties() {
		// in alphabetical order
		return new TreeSet<>(votes.keySet());
	}

	public int getVotesFor(String party) {
		return votes.get(party);
	}

	// 2: change your state
	public void addParty(String party) {
		votes.put(party, 0);
		mvc.forEachView(View::onModelChanged);
	}

	public void removeParty(String party) {
		votes.remove(party);
		mvc.forEachView(View::onModelChanged);
	}

	public void setVoteFor(String party, int votes) {
		this.votes.put(party, votes);
		mvc.forEachView(View::onModelChanged);
	}
}