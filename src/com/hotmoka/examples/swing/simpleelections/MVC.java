package com.hotmoka.examples.swing.simpleelections;

import java.util.HashSet;
import java.util.Set;

import com.hotmoka.examples.swing.simpleelections.controller.Controller;
import com.hotmoka.examples.swing.simpleelections.model.Model;
import com.hotmoka.examples.swing.simpleelections.view.View;

public class MVC {
	public final Model model;
	public final Controller controller;
	private final Set<View> views = new HashSet<>();

	public MVC(Model model,Controller controller) {
		this.model = model;
		this.controller = controller;
		
		model.setMVC(this);
		controller.setMVC(this);
	}

	public synchronized void register(View view) {
		this.views.add(view);
	}

	public synchronized void unregister(View view) {
		this.views.remove(view);
	}

	/**
     * A task that can be executed on all views currently registered.
     */
    public interface ViewTask {

        /**
         * Applies the task to the given view.
         *
         * @param view
         */
        void process(View view);
    }

    /**
     * Applies the given task to all views currently registered.
     *
     * @param task
     */
    public synchronized void forEachView(ViewTask task) {
        // Internal iteration, preferred since we do not need
        // to expose the modifiable set of views
        for (View view: views)
            task.process(view);
    }
}