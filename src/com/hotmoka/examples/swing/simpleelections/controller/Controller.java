package com.hotmoka.examples.swing.simpleelections.controller;

import com.hotmoka.examples.swing.simpleelections.MVC;
import com.hotmoka.examples.swing.simpleelections.view.View;

public class Controller {
	private MVC mvc;

	public void setMVC(MVC mvc) {
		this.mvc = mvc;
	}

	// 1: the user did something
	public void insertParty(View view) {
		view.askForNewPartyName();
	}

	public void addParty(String party) {
		mvc.model.addParty(party);
	}

	public void removeParty(String party) {
		mvc.model.removeParty(party);
	}

	public void registerVoteFor(String party) {
		mvc.model.setVoteFor(party, mvc.model.getVotesFor(party) + 1);
	}
}