package com.hotmoka.examples.swing.simpleelections.view;

public interface View {
	// 3: change your display
	void askForNewPartyName();

	// 4: I've changed
	void onModelChanged();
}