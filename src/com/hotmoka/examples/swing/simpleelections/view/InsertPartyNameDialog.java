package com.hotmoka.examples.swing.simpleelections.view;

import java.awt.Dialog;
import java.awt.FlowLayout;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextField;

import com.hotmoka.examples.swing.simpleelections.controller.Controller;

@SuppressWarnings("serial")
class InsertPartyNameDialog extends JDialog {

	public InsertPartyNameDialog(Controller controller) {
		super((Dialog) null);

		setTitle("Insert Party Name");
		setLayout(new FlowLayout());
		add(new JLabel("Insert new party name: "));

		JTextField textField = new JTextField("nome partito");
		textField.addActionListener(e -> {
			String party = textField.getText();
			if (!party.isEmpty())
				controller.addParty(party);

			setVisible(false);
			dispose();
		});
		add(textField);

		pack();
		setVisible(true);
	}
}