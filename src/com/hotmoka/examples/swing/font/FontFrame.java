package com.hotmoka.examples.swing.font;

import javax.swing.JFrame;

public class FontFrame extends JFrame {

	public FontFrame() {
		add(new FontComponent());
		pack();
	}

	private static final long serialVersionUID = 1L;
}
