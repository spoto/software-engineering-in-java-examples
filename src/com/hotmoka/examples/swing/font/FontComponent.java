package com.hotmoka.examples.swing.font;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.font.FontRenderContext;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;

import javax.swing.JComponent;

public class FontComponent extends JComponent {
	public final static int DEFAULT_WIDTH = 400;
	public final static int DEFAULT_HEIGHT = 400;
	private final static String MESSAGE = "Hello World!";
	
	@Override
	protected void paintComponent(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;

		drawMessage(g2);
		drawBaseline(g2);
		drawBoundingRect(g2);
	}

	private Rectangle2D getTextBounds(Graphics2D g2) {
		FontRenderContext context = g2.getFontRenderContext();

		return g2.getFont().getStringBounds(MESSAGE, context);
	}

	private double getStartX(Rectangle2D bounds) {
		return (getWidth() - bounds.getWidth()) / 2;
	}

	private double getStartY(Rectangle2D bounds) {
		return (getHeight() - bounds.getHeight()) / 2;
	}

	private double getBaseline(Rectangle2D bounds) {
		double y = (getHeight() - bounds.getHeight()) / 2;
		
		// aggiungiamo l'ascent a y per raggiungere la baseline
		double ascent = -bounds.getY();

		return y + ascent;
	}

	private void drawMessage(Graphics2D g2) {
		Font font = new Font("Serif", Font.BOLD, 36);
		g2.setFont(font);
		Rectangle2D bounds = getTextBounds(g2);
		g2.setPaint(Color.BLACK);
		g2.drawString(MESSAGE, (int) getStartX(bounds), (int) getBaseline(bounds));
	}

	private void drawBaseline(Graphics2D g2) {
		Rectangle2D bounds = getTextBounds(g2);
		double baseline = getBaseline(bounds);
		double x = getStartX(bounds);
		g2.setPaint(Color.LIGHT_GRAY);
		g2.draw(new Line2D.Double(x, baseline, x + bounds.getWidth(), baseline));		
	}

	private void drawBoundingRect(Graphics2D g2) {
		Rectangle2D bounds = getTextBounds(g2);
		double x = getStartX(bounds);
		double y = getStartY(bounds);
		g2.setPaint(Color.LIGHT_GRAY);
		g2.draw(new Rectangle2D.Double(x, y, bounds.getWidth(), bounds.getHeight()));
	}

	@Override
	public Dimension getPreferredSize() {
		return new Dimension(DEFAULT_WIDTH, DEFAULT_HEIGHT);
	}

	private static final long serialVersionUID = 1L;
}
