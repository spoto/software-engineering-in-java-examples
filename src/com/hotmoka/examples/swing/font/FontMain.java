package com.hotmoka.examples.swing.font;

import java.awt.EventQueue;

import javax.swing.JFrame;

public class FontMain {

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {

			@Override
			public void run() {
				JFrame frame = new FontFrame();
				frame.setTitle("Font Test");
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setVisible(true);
			}
		});
	}
}
