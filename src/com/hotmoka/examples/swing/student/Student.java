package com.hotmoka.examples.swing.student;

public class Student {
	@SuppressWarnings("unused")
	private final String name;
	@SuppressWarnings("unused")
	private final String surname;
	private final int code;
	
	public Student(String name, String surname, int code){
		this.name = name;
		this.surname = surname;
		this.code = code;
	}

	@Override
	public boolean equals(Object obj) {
		return obj instanceof Student &&
				((Student) obj).code == code;
	};

	@Override
	public int hashCode() {
		return code;
	}
}