package com.hotmoka.examples.swing.listeners;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class ButtonFrameWithAnonymousListeners extends JFrame {
	private final static int DEFAULT_WIDTH = 300;
	private final static int DEFAULT_HEIGHT = 200;
	private final JPanel buttonPanel;

	public ButtonFrameWithAnonymousListeners() {
		setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
		
		buttonPanel = new JPanel();
		
		buttonPanel.add(mkButton("Yellow", Color.YELLOW));
		buttonPanel.add(mkButton("Blue", Color.BLUE));
		buttonPanel.add(mkButton("Red", Color.RED));

		add(buttonPanel);
	}

	private JButton mkButton(String label, final Color background) {
		JButton button = new JButton(label);

		button.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent event) {
				buttonPanel.setBackground(background);
			}
		});

		return button;
	}

	private static final long serialVersionUID = 1L;
}