package com.hotmoka.examples.swing.listeners;

import java.awt.Color;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class ButtonFrameWithLambdaListenersAndImplicitConstructor extends JFrame {
	private final static int DEFAULT_WIDTH = 300;
	private final static int DEFAULT_HEIGHT = 200;
	private final JPanel buttonPanel;

	public ButtonFrameWithLambdaListenersAndImplicitConstructor() {
		setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
		
		buttonPanel = new JPanel();
		
		buttonPanel.add(mkButton("Yellow", Color.YELLOW));
		buttonPanel.add(mkButton("Blue", Color.BLUE));
		buttonPanel.add(mkButton("Red", Color.RED));

		add(buttonPanel);
	}

	@SuppressWarnings("serial")
	private JButton mkButton(String label, final Color background) {
		return new JButton(label) {{
			addActionListener(event -> buttonPanel.setBackground(background));
		}};
	}

	private static final long serialVersionUID = 1L;
}