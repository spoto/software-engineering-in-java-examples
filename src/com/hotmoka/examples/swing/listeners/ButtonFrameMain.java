package com.hotmoka.examples.swing.listeners;

import java.awt.EventQueue;

import javax.swing.JFrame;

public class ButtonFrameMain {

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {

			@Override
			public void run() {
				JFrame frame = new ButtonFrameWithLambdaListenersAndImplicitConstructor();
				frame.setTitle("Listeners Test");
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setVisible(true);
			}
		});
	}
}
