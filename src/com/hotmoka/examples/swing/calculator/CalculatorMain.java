package com.hotmoka.examples.swing.calculator;

import java.awt.EventQueue;

import javax.swing.JFrame;

public class CalculatorMain {

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {

			@Override
			public void run() {
				JFrame frame = new CalculatorFrame();
				frame.setTitle("Calculator");
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setVisible(true);
			}
		});
	}
}
