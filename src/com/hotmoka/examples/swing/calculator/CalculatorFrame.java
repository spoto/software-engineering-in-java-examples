package com.hotmoka.examples.swing.calculator;

import javax.swing.JFrame;

public class CalculatorFrame extends JFrame {

	public CalculatorFrame() {
		add(new CalculatorPanel());
		pack();
	}

	private static final long serialVersionUID = 1L;
}
