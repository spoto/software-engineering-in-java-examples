package com.hotmoka.examples.swing.mouse;

import javax.swing.JFrame;

public class MouseFrame extends JFrame {

	public MouseFrame() {
		add(new MouseComponent());
		pack();
	}

	private static final long serialVersionUID = 1L;
}
