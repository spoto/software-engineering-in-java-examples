package com.hotmoka.examples.swing.mouse;

import java.awt.EventQueue;

import javax.swing.JFrame;

public class MouseMain {

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {

			@Override
			public void run() {
				JFrame frame = new MouseFrame();
				frame.setTitle("Mouse Test");
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setVisible(true);
			}
		});
	}
}
