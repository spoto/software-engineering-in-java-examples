package com.hotmoka.examples.swing.mouse;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComponent;

public class MouseComponent extends JComponent {
	private final static int WIDTH = 500;
	private final static int HEIGHT = 400;
	private final static int SIDE_LENGTH = 10;

	private final List<Rectangle2D> squares = new ArrayList<>();
	private Rectangle2D current;

	public MouseComponent() {
		addMouseListener(new MouseHandler());
		addMouseMotionListener(new MouseMotionHandler());
	}

	@Override
	protected void paintComponent(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;

		for (Rectangle2D square: squares)
			g2.draw(square);
	}

	@Override
	public Dimension getPreferredSize() {
		return new Dimension(WIDTH, HEIGHT);
	}

	private Rectangle2D find(Point2D at) {
		for (Rectangle2D square: squares)
			if (square.contains(at))
				return square;

		return null;
	}

	private void addCurrent(Point2D where) {
		double x = where.getX();
		double y = where.getY();

		current = new Rectangle2D.Double(x - SIDE_LENGTH / 2, y - SIDE_LENGTH / 2, SIDE_LENGTH, SIDE_LENGTH);
		squares.add(current);

		repaint();
	}

	private void removeCurrent() {
		squares.remove(current);
		current = null;

		repaint();
	}

	private static final long serialVersionUID = 1L;

	private class MouseHandler extends MouseAdapter {

		@Override
		public void mousePressed(MouseEvent event) {
			current = find(event.getPoint());
			if (current == null)
				addCurrent(event.getPoint());
		}

		@Override
		public void mouseClicked(MouseEvent event) {
			current = find(event.getPoint());
			if (current != null && event.getClickCount() >= 2)
				removeCurrent();
		}
	}

	private class MouseMotionHandler implements MouseMotionListener {

		@Override
		public void mouseMoved(MouseEvent event) {
			if (find(event.getPoint()) == null)
				setCursor(Cursor.getDefaultCursor());
			else
				setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
		}

		@Override
		public void mouseDragged(MouseEvent event) {
			if (current != null) {
				int x = event.getX();
				int y = event.getY();

				current.setFrame(x - SIDE_LENGTH / 2, y - SIDE_LENGTH / 2, SIDE_LENGTH, SIDE_LENGTH);

				repaint();
			}
		}
	}
}