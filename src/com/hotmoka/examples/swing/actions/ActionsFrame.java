package com.hotmoka.examples.swing.actions;

import java.awt.Color;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

public class ActionsFrame extends JFrame {

	private final JPanel buttonPanel;

	public ActionsFrame() {
		this.buttonPanel = new JPanel();
		Action intoYellow = new ColorAction("Yellow", new ImageIcon("img/yellow-ball.png"), Color.YELLOW);
		Action intoBlue = new ColorAction("Blue", new ImageIcon("img/blue-ball.png"), Color.BLUE);
		Action intoRed = new ColorAction("Red", new ImageIcon("img/red-ball.png"), Color.RED);

		buttonPanel.add(new JButton(intoYellow));
		buttonPanel.add(new JButton(intoBlue));
		buttonPanel.add(new JButton(intoRed));

		add(buttonPanel);
		InputMap imap = buttonPanel.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
		imap.put(KeyStroke.getKeyStroke("ctrl Y"), "panel.yellow");
		imap.put(KeyStroke.getKeyStroke("ctrl B"), "panel.blue");
		imap.put(KeyStroke.getKeyStroke("ctrl R"), "panel.red");

		ActionMap amap = buttonPanel.getActionMap();
		amap.put("panel.yellow", intoYellow);
		amap.put("panel.blue", intoBlue);
		amap.put("panel.red", intoRed);

		pack();
	}

	private class ColorAction extends AbstractAction {
		private ColorAction(String name, Icon icon, Color color) {
			putValue(Action.NAME, name);
			putValue(Action.SMALL_ICON, icon);
			putValue(Action.SHORT_DESCRIPTION, "Set panel color to " + name.toLowerCase());
			putValue("color", color);
		}

		@Override
		public void actionPerformed(ActionEvent event) {
			buttonPanel.setBackground((Color) getValue("color"));
		}

		private static final long serialVersionUID = 1L;
	}

	private static final long serialVersionUID = 1L;
}
