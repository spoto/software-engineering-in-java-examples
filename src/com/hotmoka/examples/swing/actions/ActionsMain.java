package com.hotmoka.examples.swing.actions;

import java.awt.EventQueue;

import javax.swing.JFrame;

public class ActionsMain {

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {

			@Override
			public void run() {
				JFrame frame = new ActionsFrame();
				frame.setTitle("Actions Test");
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setVisible(true);
			}
		});
	}
}
