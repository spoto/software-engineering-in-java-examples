package com.hotmoka.examples.swing.laf;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

public class LafFrame extends JFrame {

	public LafFrame() {
		JPanel panel = new JPanel();
		for (UIManager.LookAndFeelInfo info: UIManager.getInstalledLookAndFeels())
			mkButton(info, panel);

		add(panel);
		pack();
	}

	private void mkButton(LookAndFeelInfo info, JPanel panel) {
		JButton button = new JButton(info.getName());
		panel.add(button);
		button.setToolTipText(info.getClassName());
		button.addActionListener(event ->
		{
			try {
				UIManager.setLookAndFeel(info.getClassName());
				SwingUtilities.updateComponentTreeUI(this);
				pack();
			}
			catch (Exception e) {}
		});
	}

	private static final long serialVersionUID = 1L;
}
