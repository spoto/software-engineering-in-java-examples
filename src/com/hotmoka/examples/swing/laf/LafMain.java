package com.hotmoka.examples.swing.laf;

import java.awt.EventQueue;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

public class LafMain {

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {

			@Override
			public void run() {
				JFrame frame = new LafFrame();
				frame.setTitle("Look and Feel Test");
				//frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setVisible(true);
				frame.addWindowListener(new WindowAdapter() {

					@Override
					public void windowClosing(WindowEvent event) {
						System.exit(0);
					}
				});
			}
		});
	}
}
