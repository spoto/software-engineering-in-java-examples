package com.hotmoka.examples.swing.puzzle15.view;

import java.awt.BorderLayout;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

import com.hotmoka.frames.MVCFrame;
import com.hotmoka.puzzle15.controller.Controller;
import com.hotmoka.puzzle15.controller.Puzzle15Controller;
import com.hotmoka.puzzle15.model.LongBackedConfiguration;
import com.hotmoka.puzzle15.model.Model;
import com.hotmoka.puzzle15.model.TilesModel;
import com.hotmoka.puzzle15.view.View;

@SuppressWarnings("serial")
public class Puzzle15Frame extends MVCFrame<Model, View, Controller> implements View {
	private final TilesPanel panel;

	public Puzzle15Frame() {
		super(new TilesModel(new LongBackedConfiguration()), new Puzzle15Controller());

		setTitle("Puzzle 15");
		panel = addTiles();
		addControlButtons();
		getController().randomize();
		setIconImage(new ImageIcon("img/puzzle15.jpg").getImage());

		pack();
	}

	private void addControlButtons() {
		JPanel panel = new JPanel();

		JButton randomize = new JButton("Randomize");
		randomize.addActionListener(event -> getController().randomize());
		panel.add(randomize);

		JButton hint = new JButton("Hint");
		hint.addActionListener(event -> getController().giveHint());
		panel.add(hint);

		add(panel, BorderLayout.NORTH);
	}

	private TilesPanel addTiles() {
		TilesPanel panel = new TilesPanel(getModel(), getController());
		add(panel, BorderLayout.CENTER);

		return panel;
	}

	@Override
	public void onStartGivingHint() {}

	@Override
	public void onStopGivingHint() {}

	@Override
	public void onModelChanged() {
		panel.onModelChanged(getModel());
		if (getController().isSolved())
			new SolvedDialog(this, getController()).setVisible(true);
	}
}


