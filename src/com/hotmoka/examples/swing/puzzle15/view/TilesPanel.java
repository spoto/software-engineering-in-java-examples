package com.hotmoka.examples.swing.puzzle15.view;

import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JPanel;

import com.hotmoka.puzzle15.controller.Controller;
import com.hotmoka.puzzle15.model.Model;

@SuppressWarnings("serial")
public class TilesPanel extends JPanel {
	private final JButton[][] buttons = new JButton[4][4];

	public TilesPanel(Model model, Controller controller) {
		setLayout(new GridLayout(4, 4));

		for (int y = 0; y < 4; y++)
			for (int x = 0; x < 4; x++)
				add(buttons[x][y] = mkButton(x, y, model.at(x, y), controller));
	}

	private JButton mkButton(int x, int y, int value, Controller controller) {
		JButton button = new JButton(value == 0 ? "" : String.valueOf(value));
		button.addActionListener(event -> controller.clickAt(x, y));

		return button;
	}

	public void onModelChanged(Model model) {
		for (int y = 0; y < 4; y++)
			for (int x = 0; x < 4; x++)
				buttons[x][y].setText(model.at(x, y) == 0 ?
					"" : String.valueOf(model.at(x, y)));
	}
}


