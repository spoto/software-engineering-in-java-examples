package com.hotmoka.examples.swing.borders;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.Border;

public class BordersFrame extends JFrame {

	private final JPanel demoPanel;
	private final JPanel buttonPanel;
	private final ButtonGroup group;

    public BordersFrame() {
		setTitle("Borders Test");

		buttonPanel = new JPanel();
		demoPanel = new JPanel();
		group = new ButtonGroup();

		addRadioButton("Lowered bevel", BorderFactory.createLoweredBevelBorder());
		addRadioButton("Raised bevel", BorderFactory.createRaisedBevelBorder());
		addRadioButton("Etched", BorderFactory.createEtchedBorder());
		addRadioButton("Line", BorderFactory.createLineBorder(Color.BLUE));
		addRadioButton("Matte", BorderFactory.createMatteBorder(10, 10, 10, 10, Color.BLUE));
		addRadioButton("Empty", BorderFactory.createEmptyBorder());

		Border etched = BorderFactory.createEtchedBorder();
		Border titled = BorderFactory.createTitledBorder(etched, "Border Types");
		buttonPanel.setBorder(titled);

		setLayout(new GridLayout(2, 1));
		add(buttonPanel);
		add(demoPanel);

		pack();
    }

	private void addRadioButton(String name, Border border) {
		JRadioButton button = new JRadioButton(name);
		button.addActionListener(event -> demoPanel.setBorder(border));
		group.add(button);
		buttonPanel.add(button);
	}

	private static final long serialVersionUID = 1L;
}