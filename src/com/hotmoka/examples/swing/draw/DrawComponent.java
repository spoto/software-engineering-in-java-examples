package com.hotmoka.examples.swing.draw;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;

import javax.swing.JComponent;

public class DrawComponent extends JComponent {
	public final static int DEFAULT_WIDTH = 400;
	public final static int DEFAULT_HEIGHT = 400;
	
	@Override
	protected void paintComponent(Graphics g) {
		// questo è vero da Java 1.2 in poi
		Graphics2D g2 = (Graphics2D) g;
		
		Rectangle2D rect = getRect();
		g2.draw(rect);
		g2.draw(getEnclosedEllipse(rect));
		g2.draw(getDiagonalLine(rect));
		g2.draw(getCircleWithSameCenter(rect));
	}

	private Line2D getDiagonalLine(Rectangle2D rect) {
		double leftX = rect.getX();
		double topY = rect.getY();
		double rightX = leftX + rect.getWidth();
		double bottomY = topY + rect.getHeight();

		return new Line2D.Double(leftX, topY, rightX, bottomY);
	}

	private Rectangle2D getRect() {
		double leftX = 100;
		double topY = 100;
		double width = 200;
		double height = 150;

		return new Rectangle2D.Double(leftX, topY, width, height);
	}

	private Ellipse2D getEnclosedEllipse(Rectangle2D rect) {
		Ellipse2D ellipse = new Ellipse2D.Double();
		ellipse.setFrame(rect);
		return ellipse;
	}

	private Ellipse2D getCircleWithSameCenter(Rectangle2D rect) {
		double centerX = rect.getCenterX();
		double centerY = rect.getCenterY();
		double radius = 150;
		
		Ellipse2D circle = new Ellipse2D.Double();
		circle.setFrameFromCenter(centerX, centerY, centerX + radius, centerY + radius);

		return circle;
	}

	@Override
	public Dimension getPreferredSize() {
		return new Dimension(DEFAULT_WIDTH, DEFAULT_HEIGHT);
	}

	private static final long serialVersionUID = 1L;
}
