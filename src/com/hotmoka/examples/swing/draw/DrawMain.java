package com.hotmoka.examples.swing.draw;

import java.awt.EventQueue;

import javax.swing.JFrame;

public class DrawMain {

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {

			@Override
			public void run() {
				JFrame frame = new DrawFrame();
				frame.setTitle("Draw Test");
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setVisible(true);
			}
		});
	}
}
