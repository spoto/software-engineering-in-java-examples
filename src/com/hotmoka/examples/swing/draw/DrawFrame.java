package com.hotmoka.examples.swing.draw;

import javax.swing.JFrame;

public class DrawFrame extends JFrame {

	public DrawFrame() {
		add(new DrawComponent());
		pack();
	}

	private static final long serialVersionUID = 1L;
}
