package com.hotmoka.examples.swing.elections.model;

import java.util.HashMap;
import java.util.Map;

import com.hotmoka.examples.swing.elections.controller.Controller;
import com.hotmoka.examples.swing.elections.view.View;

public class Model extends com.hotmoka.mvc.Model<Model, View, Controller> {
	private final Map<String, Integer> votes = new HashMap<>();

	// 5: I need your state information
	public Iterable<String> getParties() {
		return votes.keySet();
	}

	public int getVotesFor(String party) {
		return votes.get(party);
	}

	// 2: change your state
	public void addParty(String party) {
		votes.put(party, 0);

		forEachView(View::onModelChanged);
	}

	public void removeParty(String party) {
		votes.remove(party);

		forEachView(View::onModelChanged);
	}

	public void setVoteFor(String party, int votes) {
		this.votes.put(party, votes);

		forEachView(View::onModelChanged);
	}
}