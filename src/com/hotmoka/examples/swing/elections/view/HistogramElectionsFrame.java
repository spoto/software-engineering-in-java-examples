package com.hotmoka.examples.swing.elections.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.hotmoka.examples.swing.elections.controller.Controller;
import com.hotmoka.examples.swing.elections.model.Model;
import com.hotmoka.frames.MVCFrame;

@SuppressWarnings("serial")
public class HistogramElectionsFrame extends MVCFrame<Model, View, Controller> implements View {

	private final JPanel scores;

	public HistogramElectionsFrame(Model model, Controller controller) {
		super(model, controller);

		setPreferredSize(new Dimension(450, 300));
		setTitle("Histogram Elections");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		this.scores = buildWidgets();

		onModelChanged();
	}

	private JPanel buildWidgets() {
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());

		JPanel scores = new JPanel();
		scores.setLayout(new GridLayout(0, 2));
		panel.add(scores, BorderLayout.NORTH);

		add(new JScrollPane(panel));

		return scores;
	}

	@Override
	public void onModelChanged() {
		Model model = getModel();

		int totalVotes = 0;
		for (String party: model.getParties())
			totalVotes += model.getVotesFor(party);

		scores.removeAll();
		for (String party: model.getParties()) {
			JButton label = new JButton(party);
			label.addActionListener(e -> getController().registerVoteFor(party));
			scores.add(label);
			scores.add(new Histogram((float) model.getVotesFor(party) / totalVotes));
		}
		
		pack();
	}
}