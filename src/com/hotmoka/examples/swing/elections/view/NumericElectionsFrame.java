package com.hotmoka.examples.swing.elections.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.hotmoka.examples.swing.elections.controller.Controller;
import com.hotmoka.examples.swing.elections.model.Model;
import com.hotmoka.frames.MVCFrame;

@SuppressWarnings("serial")
public class NumericElectionsFrame extends MVCFrame<Model, View, Controller> implements View {

	private final JPanel scores;

	public NumericElectionsFrame(Model model, Controller controller) {
		super(model, controller);

		setPreferredSize(new Dimension(430, 300));
		setTitle("Numeric Elections");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		this.scores = buildWidgets();

		onModelChanged();
	}

	private JPanel buildWidgets() {
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());

		JPanel scores = new JPanel();
		scores.setLayout(new GridLayout(0, 2));
		panel.add(scores, BorderLayout.NORTH);

		JButton addParty = new JButton("+");
		addParty.addActionListener(e -> getController().insertNewParty());
		panel.add(addParty, BorderLayout.SOUTH);

		add(new JScrollPane(panel));

		return scores;
	}

	@Override
	public void onModelChanged() {
		Model model = getModel();

		scores.removeAll();
		for (String party: model.getParties()) {
			JButton label = new JButton(party + ": " + model.getVotesFor(party) + " votes");
			label.addActionListener(e -> getController().registerVoteFor(party));
			scores.add(label);
			JButton remove = new JButton("-");
			remove.addActionListener(e -> getController().removeParty(party));
			scores.add(remove);
		}
		
		pack();
	}
}