package com.hotmoka.mvc;

public interface View<M extends Model<M,V,C>,
                      V extends View<M,V,C>,
                      C extends Controller<M,V,C>> {
}