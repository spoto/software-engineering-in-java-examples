package com.hotmoka.mvc;

import java.util.HashSet;
import java.util.Set;

public final class MVC<M extends Model<M,V,C>,
                       V extends View<M,V,C>,
                       C extends Controller<M,V,C>> {
	public final M model;
	public final Set<V> views = new HashSet<>();
	public final C controller;
	public final int id;
	private static int counter;
	private final static Set<MVC<?,?,?>> mvcs = new HashSet<>();

	public MVC(M model, V view, C controller) {
		this.model = model;
		if (view != null)
			this.views.add(view);
		this.controller = controller;
		this.id = counter++;

		model.setMVC(this);
		controller.setMVC(this);

		mvcs.add(this);
	}

	@SuppressWarnings("unchecked")
	public static <M extends Model<M,V,C>,
				   V extends View<M,V,C>,
				   C extends Controller<M,V,C>> MVC<M,V,C> getFor(M model, C controller) {

		for (MVC<?,?,?> other: mvcs)
			if (other.model == model && other.controller == controller)
				return (MVC<M,V,C>) other;

		return new MVC<M,V,C>(model, null, controller);
	}

	public void addView(V view) {
		if (view != null)
			this.views.add(view);
	}

	public void removeView(V view) {
		if (view != null)
			this.views.remove(view);
	}

	@SuppressWarnings("unchecked")
	public static <M extends Model<M,V,C>,
	               V extends View<M,V,C>,
	               C extends Controller<M,V,C>>
		MVC<M,V,C> getNumbered(int id) {

		for (MVC<?,?,?> mvc: mvcs)
			if (mvc.id == id)
				return (MVC<M, V, C>) mvc;

		return null;
	}

	public void remove() {
		mvcs.remove(this);
	}
}