package com.hotmoka.mvc;

public abstract class Model<M extends Model<M,V,C>,
                            V extends View<M,V,C>,
                            C extends Controller<M,V,C>> {
	private MVC<M,V,C> mvc;

	void setMVC(MVC<M,V,C> mvc) {
		this.mvc = mvc;
	}

	@SuppressWarnings("unchecked")
	public final void forEachView(ViewProcessor<M, V, C> processor) {
		for (View<M, V, C> view: mvc.views)
			processor.process((V) view);
	}
}