package com.hotmoka.puzzle15.moves;

import com.hotmoka.puzzle15.controller.Controller;
import com.hotmoka.puzzle15.model.Model;

public class Mover {
	private final Controller controller;

	public Mover(Controller controller) {
		this.controller = controller;
	}

	public boolean isSolved() {
		return new Rules(controller.getModel().getConfiguration()).isSolved();
	}

	public void moveAt(int x, int y) {
		Model model = controller.getModel();
		model.setConfiguration(new Rules(model.getConfiguration()).tryAt(x, y));
	}

	public void moveUpwardsAt(int x, int y) {
		Model model = controller.getModel();
		model.setConfiguration(new Rules(model.getConfiguration()).tryUp(x, y));
	}

	public void moveDownwardsAt(int x, int y) {
		Model model = controller.getModel();
		model.setConfiguration(new Rules(model.getConfiguration()).tryDown(x, y));
	}

	public void moveLeftwardsAt(int x, int y) {
		Model model = controller.getModel();
		model.setConfiguration(new Rules(model.getConfiguration()).tryLeft(x, y));
	}

	public void moveRightwardsAt(int x, int y) {
		Model model = controller.getModel();
		model.setConfiguration(new Rules(model.getConfiguration()).tryRight(x, y));
	}

	public void randomize() {
		Model model = controller.getModel();
		model.setConfiguration(new Rules(model.getConfiguration()).randomize(1000));
	}
}