package com.hotmoka.puzzle15.controller;

import com.hotmoka.puzzle15.ai.Solver;
import com.hotmoka.puzzle15.ai.Solver.SolutionLookupListener;
import com.hotmoka.puzzle15.moves.Mover;
import com.hotmoka.puzzle15.view.View;

public class Puzzle15Controller extends Controller {
	private final Mover mover;
	private final Solver solver;

	public Puzzle15Controller() {
		this.mover = new Mover(this);
		this.solver = new Solver(this);
	}

	@Override
	public void giveHint() {
		if (!mover.isSolved()) {
			solver.step(new SolutionLookupListener() {
				
				@Override
				public void onStartThinking() {
					forEachView(View::onStartGivingHint);
				}

				@Override
				public void onStopThinking() {
					forEachView(View::onStopGivingHint);
				}
			});
		}
	}

	@Override
	public boolean isGivingHint() {
		return solver.isThinking();
	}

	@Override
	public boolean isSolved() {
		return mover.isSolved();
	}

	@Override
	public void randomize() {
		do {
			mover.randomize();
		}
		while (mover.isSolved());

		getModel().setMovesCount(0);
	}

	@Override
	public void clickAt(int x, int y) {
		mover.moveAt(x, y);
	}
}