package com.hotmoka.puzzle15.controller;

import com.hotmoka.puzzle15.model.Model;
import com.hotmoka.puzzle15.view.View;

public abstract class Controller extends com.hotmoka.mvc.Controller<Model, View, Controller>{
	// 1: the user did something
	public abstract void clickAt(int x, int y);
	public abstract void randomize();
	public abstract void giveHint();
	public abstract boolean isGivingHint();
	public abstract boolean isSolved();
}