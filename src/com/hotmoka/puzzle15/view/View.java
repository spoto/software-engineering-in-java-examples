package com.hotmoka.puzzle15.view;

import com.hotmoka.puzzle15.controller.Controller;
import com.hotmoka.puzzle15.model.Model;

public interface View extends com.hotmoka.mvc.View<Model, View, Controller> {
	// 3: change your display
	void onStartGivingHint();
	void onStopGivingHint();

	// 4: I've changed
	void onModelChanged();
}