package com.hotmoka.puzzle15.ai;

import java.util.concurrent.atomic.AtomicBoolean;

import com.hotmoka.puzzle15.controller.Controller;
import com.hotmoka.puzzle15.model.Model;

public class Solver {
	private final AtomicBoolean isThinking = new AtomicBoolean(false);
	private Steps steps;
	private final Controller controller;

	public Solver(Controller controller) {
		this.controller = controller;
	}

	public void step(final SolutionLookupListener listener) {
		Model model = controller.getModel();

		if (steps == null || !steps.head.equals(model.getConfiguration())) {
			if (isThinking.getAndSet(true) == false) {
				listener.onStartThinking();

				new Thread() {

					@Override
					public void run() {
						steps = new Solution(controller.getModel().getConfiguration()).getSequenceOfSteps().tail;
						isThinking.set(false);
						controller.getModel().setConfiguration(steps.head);
						listener.onStopThinking();
					}
				}.start();
			}
		}
		else {
			steps = steps.tail;
			model.setConfiguration(steps.head);
		}
	}

	public boolean isThinking() {
		return isThinking.get();
	}

	public interface SolutionLookupListener {
		void onStartThinking();
		void onStopThinking();
	}
}