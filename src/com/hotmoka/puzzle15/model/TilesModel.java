package com.hotmoka.puzzle15.model;

import com.hotmoka.puzzle15.view.View;

public class TilesModel extends Model {
	private Configuration configuration;
	private int movesCount;

	public TilesModel(Configuration configuration) {
		this.configuration = configuration;
	}

	@Override
	public int at(int x, int y) {
		return configuration.at(x, y);
	}

	@Override
	public Configuration getConfiguration() {
		return configuration;
	}

	@Override
	public void setConfiguration(Configuration configuration) {
		if (this.configuration != configuration) {
			this.configuration = configuration;
			setMovesCount(movesCount + 1);
		}
	};

	@Override
	public int getMovesCount() {
		return movesCount;
	}

	@Override
	public void setMovesCount(int movesCount) {
		if (this.movesCount != movesCount) {
			this.movesCount = movesCount;
			forEachView(View::onModelChanged);
		}
	}
}