package com.hotmoka.puzzle15.model;

import com.hotmoka.puzzle15.controller.Controller;
import com.hotmoka.puzzle15.view.View;

public abstract class Model extends com.hotmoka.mvc.Model<Model, View, Controller> {
	// 5: I need your state information
	public abstract int at(int x, int y);
	public abstract Configuration getConfiguration();
	public abstract int getMovesCount();

	// 2: change your state
	public abstract void setConfiguration(Configuration configuration);
	public abstract void setMovesCount(int movesCount);
}