package com.hotmoka.frames;

import javax.swing.JPanel;

import com.hotmoka.mvc.Controller;
import com.hotmoka.mvc.MVC;
import com.hotmoka.mvc.Model;
import com.hotmoka.mvc.View;

@SuppressWarnings("serial")
public abstract class MVCPanel<M extends Model<M,V,C>,
                               V extends View<M,V,C>,
                               C extends Controller<M,V,C>>
		extends JPanel implements View<M,V,C> {

	private final MVC<M,V,C> mvc;
	
	@SuppressWarnings("unchecked")
	protected MVCPanel(M model, C controller) {
		mvc = new MVC<M, V, C>(model, (V) this, controller);
	}

	public final M getModel() {
		return mvc.model;
	}

	public final C getController() {
		return mvc.controller;
	}
}