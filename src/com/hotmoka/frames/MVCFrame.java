package com.hotmoka.frames;

import javax.swing.JFrame;

import com.hotmoka.mvc.Controller;
import com.hotmoka.mvc.MVC;
import com.hotmoka.mvc.Model;
import com.hotmoka.mvc.View;

@SuppressWarnings("serial")
public abstract class MVCFrame<M extends Model<M,V,C>,
                               V extends View<M,V,C>,
                               C extends Controller<M,V,C>>
		extends JFrame implements View<M,V,C> {

	private final MVC<M,V,C> mvc;
	
	@SuppressWarnings("unchecked")
	protected MVCFrame(M model, C controller) {
		this.mvc = MVC.getFor(model, controller);
		this.mvc.addView((V) this);
	}

	public final M getModel() {
		return mvc.model;
	}

	public final C getController() {
		return mvc.controller;
	}
}